package co.gdev.bowlingscorecalculator.model;

import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class TwoBallFrameTest {

    // TODO Write calculator tests
//    @Test
//    public void testFirstFrameNoStrikeNoSpare() {
//        TwoBallFrame tbf = TwoBallFrame.of(1, 5, 3);
//        Game mockGame = mock(Game.class);
//        int total = tbf.getTotal(mockGame);
//        assertEquals(total, 8);
//    }
//
//    @Test
//    public void testFirstFrameSpare() {
//        TwoBallFrame tbf = TwoBallFrame.of(1, 5, 5);
//        TwoBallFrame tbf2 = TwoBallFrame.of(2, 4,5);
//        Game mockGame = mock(Game.class);
//        when(mockGame.getFrameByNumber(2)).thenReturn(tbf2);
//        int total = tbf.getTotal(mockGame);
//        assertEquals(14, total);
//    }
//
//    @Test
//    public void testFirstFrameStrikeNextFrameNormal() {
//        TwoBallFrame tbf = TwoBallFrame.of(1, 10, 0);
//        TwoBallFrame tbf2 = TwoBallFrame.of(2, 4,5);
//        Game mockGame = mock(Game.class);
//        when(mockGame.getFrameByNumber(2)).thenReturn(tbf2);
//        int total = tbf.getTotal(mockGame);
//        assertEquals(19, total);
//    }
//
//    @Test
//    public void testFirstFrameStrikeNextFrameStrike() {
//        TwoBallFrame tbf = TwoBallFrame.of(1, 10, 0);
//        TwoBallFrame tbf2 = TwoBallFrame.of(2, 10,0);
//        TwoBallFrame tbf3 = TwoBallFrame.of(3, 10, 0);
//        Game mockGame = mock(Game.class);
//        when(mockGame.getFrameByNumber(2)).thenReturn(tbf2);
//        when(mockGame.getFrameByNumber(3)).thenReturn(tbf3);
//        int total = tbf.getTotal(mockGame);
//        assertEquals(30, total);
//    }
//
//    @Test
//    public void testNinthFrameSpare() {
//        Game mockGame = mock(Game.class);
//        TwoBallFrame tbf = mock(TwoBallFrame.class);
//        when(tbf.getTotal(mockGame)).thenReturn(18);
//        Frame tbf2 = TwoBallFrame.of(9, 8,2);
//        Frame tbf3 = TwoBallFrame.of(10, 10, 0);
//        when(mockGame.getFrameByNumber(8)).thenReturn(tbf);
//        when(mockGame.getFrameByNumber(10)).thenReturn(tbf3);
//
//        int total = tbf2.getTotal(mockGame);
//        assertEquals(38, total);
//    }
//
//    @Test
//    public void testNinthFrameStrike() {
//        Game mockGame = mock(Game.class);
//        TwoBallFrame tbf = mock(TwoBallFrame.class);
//        when(tbf.getTotal(mockGame)).thenReturn(18);
//        Frame tbf2 = TwoBallFrame.of(9, 10,0);
//        Frame tbf3 = ThreeBallFrame.of(10, 10, 5,0);
//        when(mockGame.getFrameByNumber(8)).thenReturn(tbf);
//        when(mockGame.getFrameByNumber(10)).thenReturn(tbf3);
//
//        int total = tbf2.getTotal(mockGame);
//        assertEquals(43, total);
//    }
//
//    @Test
//    public void testGetFrameNumber() {
//        TwoBallFrame of = TwoBallFrame.of(2, 3, 4);
//        assertEquals(of.getFrameNumber(), 2);
//    }

}
