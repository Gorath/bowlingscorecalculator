package co.gdev.bowlingscorecalculator.model;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class GameTest {

    @Test
    public void testGetNoFrames() {
        Frame mock1 = mock(TwoBallFrame.class);
        Frame mock2 = mock(TwoBallFrame.class);
        Frame mock3 = mock(TwoBallFrame.class);
        Frame mock4 = mock(TwoBallFrame.class);
        Frame mock5 = mock(TwoBallFrame.class);
        Frame mock6 = mock(TwoBallFrame.class);
        Frame mock7 = mock(TwoBallFrame.class);
        Frame mock8 = mock(TwoBallFrame.class);
        Frame mock9 = mock(TwoBallFrame.class);
        Frame mock10 = mock(ThreeBallFrame.class);

        List<Frame> frames = Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6, mock7, mock8, mock9, mock10);

        Game of = Game.of(10, frames);

        assertEquals(of.getTotalNumberOfFrames(), 10, "Should be 10 frames");

    }

    @Test
    public void testGetFrame() throws InvalidFrameFetchException {
        Frame mock1 = mock(TwoBallFrame.class);
        Frame mock2 = mock(TwoBallFrame.class);
        Frame mock3 = mock(TwoBallFrame.class);
        Frame mock4 = mock(TwoBallFrame.class);
        Frame mock5 = mock(TwoBallFrame.class);
        Frame mock6 = mock(TwoBallFrame.class);
        Frame mock7 = mock(TwoBallFrame.class);
        Frame mock8 = mock(TwoBallFrame.class);
        Frame mock9 = mock(TwoBallFrame.class);
        Frame mock10 = mock(ThreeBallFrame.class);

        List<Frame> frames = Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6, mock7, mock8, mock9, mock10);

        Game of = Game.of(10, frames);

        assertEquals(of.getFrameByNumber(1), mock1, "Should be equal");
        assertEquals(of.getFrameByNumber(2), mock2, "Should be equal");
        assertEquals(of.getFrameByNumber(3), mock3, "Should be equal");
        assertEquals(of.getFrameByNumber(4), mock4, "Should be equal");
        assertEquals(of.getFrameByNumber(5), mock5, "Should be equal");
        assertEquals(of.getFrameByNumber(6), mock6, "Should be equal");
        assertEquals(of.getFrameByNumber(7), mock7, "Should be equal");
        assertEquals(of.getFrameByNumber(8), mock8, "Should be equal");
        assertEquals(of.getFrameByNumber(9), mock9, "Should be equal");
        assertEquals(of.getFrameByNumber(10), mock10, "Should be equal");

    }

    @Test(expectedExceptions = InvalidFrameFetchException.class)
    public void getInvalidFrame() throws InvalidFrameFetchException {
        Game of = Game.of(10, Collections.singletonList(TwoBallFrame.of(1, 2, 3)));
        of.getFrameByNumber(5);
    }

    @Test
    public void testGetFrames() {
        List<Frame> input = Collections.singletonList(TwoBallFrame.of(1, 2, 3));
        Game of = Game.of(10, input);
        assertEquals(of.getFrames(), input);
    }
}
