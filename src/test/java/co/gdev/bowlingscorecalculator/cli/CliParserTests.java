package co.gdev.bowlingscorecalculator.cli;

import co.gdev.bowlingscorecalculator.input.BowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.FileBowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.StdInBowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.StringBowlingScoreInput;
import org.testng.annotations.Test;

import java.net.URL;

import static org.testng.AssertJUnit.assertTrue;

public class CliParserTests {

    @Test
    public void testFileInput() {
        URL url = this.getClass().getClassLoader().getResource("testFile.txt");
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{"-f", url.getFile()});
        assertTrue(inputSource instanceof FileBowlingScoreInput);
    }

    @Test
    public void testFileNotExist() {
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{"-f", "unknown-file"});
        assertTrue(inputSource == null);
    }

    @Test
    public void testStdIn() {
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{"-sin"});
        assertTrue(inputSource instanceof StdInBowlingScoreInput);
    }

    @Test
    public void testStringInput() {
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{"-s", "1 2 3"});
        assertTrue(inputSource instanceof StringBowlingScoreInput);
    }

    @Test
    public void testTooFewArgs() {
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{});
        assertTrue(inputSource instanceof StdInBowlingScoreInput);
    }

    @Test
    public void testTooManyArgs() {
        final BowlingScoreInput inputSource = CliParser.getInputSource(new String[]{"-sin", "-s", "something"});
        assertTrue(inputSource == null);
    }

}
