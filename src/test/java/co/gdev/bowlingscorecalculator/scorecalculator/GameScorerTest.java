package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.*;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class GameScorerTest {

    @Test
    public void testEmptyGameList() throws ScoreCalculationException {

        final FrameScoreCalculator mockCalculator1 = mock(FrameScoreCalculator.class);
        final FrameScoreCalculator mockCalculator2 = mock(FrameScoreCalculator.class);
        final List<FrameScoreCalculator> calculators = Arrays.asList(mockCalculator1, mockCalculator2);

        final TwoBallFrame mockFrame1 = mock(TwoBallFrame.class);
        final ThreeBallFrame mockFrame2 = mock(ThreeBallFrame.class);
        final List<Frame> frames = Arrays.asList(mockFrame1, mockFrame2);

        final GameContext mockGameContext = mock(GameContext.class);

        when(mockGameContext.getFrames()).thenReturn(frames);

        when(mockCalculator1.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mockCalculator2.appliesTo()).thenReturn(ThreeBallFrame.class);

        when(mockCalculator1.calculateScore(mockFrame1, mockGameContext)).thenReturn(4);
        when(mockCalculator2.calculateScore(mockFrame2, mockGameContext)).thenReturn(5);


        final GameScorer gameScorer = new GameScorer(calculators);
        final int total = gameScorer.scoreGame(mockGameContext);

        assertEquals(9, total);

    }

    @Test(expectedExceptions = ScoreCalculationException.class)
    public void appliesToTypeMismatch() throws ScoreCalculationException {

        final FrameScoreCalculator mockCalculator1 = mock(FrameScoreCalculator.class);
        final FrameScoreCalculator mockCalculator2 = mock(FrameScoreCalculator.class);
        final List<FrameScoreCalculator> calculators = Arrays.asList(mockCalculator1, mockCalculator2);

        final TwoBallFrame mockFrame1 = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);
        final List<Frame> frames = Arrays.asList(mockFrame1, mockFrame2);

        final GameContext mockGameContext = mock(GameContext.class);

        when(mockGameContext.getFrames()).thenReturn(frames);

        when(mockCalculator1.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mockCalculator2.appliesTo()).thenReturn(ThreeBallFrame.class);

        when(mockCalculator1.calculateScore(mockFrame1, mockGameContext)).thenReturn(4);
        when(mockCalculator2.calculateScore(mockFrame2, mockGameContext)).thenReturn(5);


        final GameScorer gameScorer = new GameScorer(calculators);
        gameScorer.scoreGame(mockGameContext);
    }


    @Test(expectedExceptions = ScoreCalculationException.class)
    public void differenceBetweenFramesAndCalculatorSizeCalculatorSmaller() throws ScoreCalculationException {

        final FrameScoreCalculator mockCalculator1 = mock(FrameScoreCalculator.class);
        final List<FrameScoreCalculator> calculators = Arrays.asList(mockCalculator1);

        final TwoBallFrame mockFrame1 = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);
        final List<Frame> frames = Arrays.asList(mockFrame1, mockFrame2);

        final GameContext mockGameContext = mock(GameContext.class);

        when(mockGameContext.getFrames()).thenReturn(frames);

        final GameScorer gameScorer = new GameScorer(calculators);
        gameScorer.scoreGame(mockGameContext);
    }

    @Test(expectedExceptions = ScoreCalculationException.class)
    public void differenceBetweenFramesAndCalculatorSizeFramesSmaller() throws ScoreCalculationException {

        final FrameScoreCalculator mockCalculator1 = mock(FrameScoreCalculator.class);
        final FrameScoreCalculator mockCalculator2 = mock(FrameScoreCalculator.class);
        final List<FrameScoreCalculator> calculators = Arrays.asList(mockCalculator1, mockCalculator2);

        final TwoBallFrame mockFrame1 = mock(TwoBallFrame.class);
        final List<Frame> frames = Arrays.asList(mockFrame1);

        final GameContext mockGameContext = mock(GameContext.class);

        when(mockGameContext.getFrames()).thenReturn(frames);

        final GameScorer gameScorer = new GameScorer(calculators);
        gameScorer.scoreGame(mockGameContext);
    }

}
