package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class NormalThreeBallFrameScoreCalculatorTest {

    private final NormalThreeBallFrameScoreCalculator calculator = new NormalThreeBallFrameScoreCalculator();

    @Test
    public void testThreeBallFrameReturnsSumOfAllThreeBalls() throws ScoreCalculationException {
        checkValues(10, 20, 30, 60);
    }


    @Test
    public void testThreeBallFrameReturnsSumOfAllThreeBallsAllZero() throws ScoreCalculationException {
        checkValues(0, 0, 0, 0);
    }

    @Test
    public void testThreeBallFrameReturnsSumOfAllThreeBallsFirstOne() throws ScoreCalculationException {
        checkValues(1, 0, 0, 1);
    }

    @Test
    public void testThreeBallFrameReturnsSumOfAllThreeBallsSecondOne() throws ScoreCalculationException {
        checkValues(0, 1, 0, 1);
    }

    @Test
    public void testThreeBallFrameReturnsSumOfAllThreeBallsThirdOne() throws ScoreCalculationException {
        checkValues(0, 0, 1, 1);
    }

    private void checkValues(int firstBallScore, int secondBallScore, int thirdBallScore, int sum) throws ScoreCalculationException {
        final ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockFrame.getFirstBallScore()).thenReturn(firstBallScore);
        when(mockFrame.getSecondBallScore()).thenReturn(secondBallScore);
        when(mockFrame.getThirdBallScore()).thenReturn(thirdBallScore);

        final int output = calculator.calculateScore(mockFrame, null);

        assertEquals(sum, output);
    }

    @Test
    public void checkAppliesTo() {
        final Class<? extends ThreeBallFrame> aClass = calculator.appliesTo();
        assertTrue(aClass.isAssignableFrom(ThreeBallFrame.class));
        assertTrue(ThreeBallFrame.class.isAssignableFrom(aClass));
    }

}
