package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.InvalidFrameFetchException;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class NormalTwoBallFrameScoreCalculatorTest {

    NormalTwoBallFrameScoreCalculator calculator = new NormalTwoBallFrameScoreCalculator();

    @Test
    public void testTwoBallFrameNotStrikeOrSpareReturnsSumOfBalls() throws ScoreCalculationException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockFrame.getFirstBallScore()).thenReturn(5);
        when(mockFrame.getSecondBallScore()).thenReturn(3);
        when(mockFrame.isStrike(10)).thenReturn(false);
        when(mockFrame.isSpare(10)).thenReturn(false);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(8, score);
    }

    @Test
    public void testTwoBallFrameSpareReturnsSumOfBallsPlusNextBall() throws ScoreCalculationException, InvalidFrameFetchException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(2)).thenReturn(mockFrame2);
        when(mockFrame.getFrameNumber()).thenReturn(1);
        when(mockFrame.getFirstBallScore()).thenReturn(5);
        when(mockFrame.getSecondBallScore()).thenReturn(5);
        when(mockFrame.isStrike(10)).thenReturn(false);
        when(mockFrame.isSpare(10)).thenReturn(true);
        when(mockFrame2.getFirstBallScore()).thenReturn(9);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(19, score);
    }

    @Test
    public void testTwoBallFrameStrikeReturnsSumOfBallsPlusNextTwoNextFrameTwoBallNoStrike() throws InvalidFrameFetchException, ScoreCalculationException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(2)).thenReturn(mockFrame2);
        when(mockFrame.getFrameNumber()).thenReturn(1);
        when(mockFrame.getFirstBallScore()).thenReturn(10);
        when(mockFrame.isStrike(10)).thenReturn(true);
        when(mockFrame2.isStrike(10)).thenReturn(false);
        when(mockFrame2.getFirstBallScore()).thenReturn(2);
        when(mockFrame2.getSecondBallScore()).thenReturn(3);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(15, score);
    }

    @Test
    public void testTwoBallFrameStrikeReturnsSumOfBallsPlusNextTwoNextFrameThreeBall() throws InvalidFrameFetchException, ScoreCalculationException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final ThreeBallFrame mockFrame2 = mock(ThreeBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(2)).thenReturn(mockFrame2);
        when(mockFrame.getFrameNumber()).thenReturn(1);
        when(mockFrame.getFirstBallScore()).thenReturn(10);
        when(mockFrame.isStrike(10)).thenReturn(true);
        when(mockFrame2.getFirstBallScore()).thenReturn(4);
        when(mockFrame2.getSecondBallScore()).thenReturn(5);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(19, score);
    }

    @Test
    public void testTwoBallFrameStrikeReturnsSumOfBallsPlusNextTwoTwoBallFrameNextAndThreeBallFrameAfter() throws InvalidFrameFetchException, ScoreCalculationException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);
        final ThreeBallFrame mockFrame3 = mock(ThreeBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(2)).thenReturn(mockFrame2);
        when(mockGameContext.getFrameByNumber(3)).thenReturn(mockFrame3);
        when(mockFrame.getFrameNumber()).thenReturn(1);
        when(mockFrame.getFirstBallScore()).thenReturn(10);
        when(mockFrame.isStrike(10)).thenReturn(true);
        when(mockFrame2.isStrike(10)).thenReturn(true);
        when(mockFrame2.getFirstBallScore()).thenReturn(10);
        when(mockFrame3.getFirstBallScore()).thenReturn(8);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(28, score);
    }

    @Test
    public void testTwoBallFrameStrikeReturnsSumOfBallsPlusNextTwoTwoBallFrameNextAndTwoBallFrameAfter() throws InvalidFrameFetchException, ScoreCalculationException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame3 = mock(TwoBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(2)).thenReturn(mockFrame2);
        when(mockGameContext.getFrameByNumber(3)).thenReturn(mockFrame3);
        when(mockFrame.getFrameNumber()).thenReturn(1);
        when(mockFrame.getFirstBallScore()).thenReturn(10);
        when(mockFrame.isStrike(10)).thenReturn(true);
        when(mockFrame2.isStrike(10)).thenReturn(true);
        when(mockFrame2.getFirstBallScore()).thenReturn(10);
        when(mockFrame3.getFirstBallScore()).thenReturn(6);

        final int score = calculator.calculateScore(mockFrame, mockGameContext);

        assertEquals(26, score);
    }

    @Test(expectedExceptions = ScoreCalculationException.class)
    public void testFrameDoesntExistThrowsScoreCalculationException() throws ScoreCalculationException, InvalidFrameFetchException {
        final GameContext mockGameContext = mock(GameContext.class);
        final TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        final TwoBallFrame mockFrame2 = mock(TwoBallFrame.class);

        when(mockGameContext.getNumberOfPins()).thenReturn(10);
        when(mockGameContext.getFrameByNumber(5)).thenThrow(InvalidFrameFetchException.class);
        when(mockFrame.getFrameNumber()).thenReturn(4);
        when(mockFrame.getFirstBallScore()).thenReturn(5);
        when(mockFrame.getSecondBallScore()).thenReturn(5);
        when(mockFrame.isStrike(10)).thenReturn(false);
        when(mockFrame.isSpare(10)).thenReturn(true);
        when(mockFrame2.getFirstBallScore()).thenReturn(9);

        calculator.calculateScore(mockFrame, mockGameContext);
    }


    @Test
    public void checkAppliesTo() {
        final Class<? extends TwoBallFrame> aClass = calculator.appliesTo();
        assertTrue(aClass.isAssignableFrom(TwoBallFrame.class));
        assertTrue(TwoBallFrame.class.isAssignableFrom(aClass));
    }
}
