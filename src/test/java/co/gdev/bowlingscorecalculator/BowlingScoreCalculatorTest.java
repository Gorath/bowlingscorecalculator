package co.gdev.bowlingscorecalculator;

import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.parser.GameConstructionException;
import co.gdev.bowlingscorecalculator.parser.GameFactory;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.CorrectBowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.ErrorBowlingScoreResult;
import co.gdev.bowlingscorecalculator.scorecalculator.GameScorer;
import co.gdev.bowlingscorecalculator.scorecalculator.ScoreCalculationException;
import co.gdev.bowlingscorecalculator.validation.ValidationController;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class BowlingScoreCalculatorTest {

    @Test
    public void testNoException() throws GameConstructionException, ScoreCalculationException {
        final GameFactory mockFactory = mock(GameFactory.class);
        final ValidationController mockValidation = mock(ValidationController.class);
        final GameScorer mockScorer = mock(GameScorer.class);
        final Game mockGame = mock(Game.class);

        when(mockFactory.create(any())).thenReturn(mockGame);
        when(mockScorer.scoreGame(mockGame)).thenReturn(10);

        final BowlingScoreCalculator bowlingScoreCalculator = new BowlingScoreCalculator(
                mockFactory, mockValidation, mockScorer);
        final BowlingScoreResult result = bowlingScoreCalculator.calculateScore("1 2 3");

        assertTrue(result instanceof CorrectBowlingScoreResult);
        CorrectBowlingScoreResult cresult = (CorrectBowlingScoreResult) result;
        assertEquals(cresult.getOutput(), 10);

    }

    @Test
    public void testInvalidInputReturnsErrorCase() {
        final GameFactory mockFactory = mock(GameFactory.class);
        final ValidationController mockValidation = mock(ValidationController.class);
        final GameScorer mockScorer = mock(GameScorer.class);

        final BowlingScoreCalculator bowlingScoreCalculator = new BowlingScoreCalculator(
                mockFactory, mockValidation, mockScorer);
        final BowlingScoreResult result = bowlingScoreCalculator.calculateScore("a b c");

        assertTrue(result instanceof ErrorBowlingScoreResult);
        ErrorBowlingScoreResult eresult = (ErrorBowlingScoreResult) result;
        assertTrue(eresult.getException() instanceof BowlingScoreInvalidInputException);
    }


    @Test
    public void testEmptyInputReturnsErrorCase() {
        final GameFactory mockFactory = mock(GameFactory.class);
        final ValidationController mockValidation = mock(ValidationController.class);
        final GameScorer mockScorer = mock(GameScorer.class);

        final BowlingScoreCalculator bowlingScoreCalculator = new BowlingScoreCalculator(
                mockFactory, mockValidation, mockScorer);
        final BowlingScoreResult result = bowlingScoreCalculator.calculateScore("     ");

        assertTrue(result instanceof ErrorBowlingScoreResult);
        ErrorBowlingScoreResult eresult = (ErrorBowlingScoreResult) result;
        assertTrue(eresult.getException() instanceof BowlingScoreInvalidInputException);
    }

}
