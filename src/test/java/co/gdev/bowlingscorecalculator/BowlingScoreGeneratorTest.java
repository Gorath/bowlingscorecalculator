package co.gdev.bowlingscorecalculator;

import co.gdev.bowlingscorecalculator.input.BowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.BowlingScoreInputException;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.CorrectBowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.ErrorBowlingScoreResult;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertTrue;

public class BowlingScoreGeneratorTest {

    // TODO Should mock the stuff here
    @Test
    public void testInputClosed() throws BowlingScoreInputException {
        final BowlingScoreInput mInput = mock(BowlingScoreInput.class);
        when(mInput.endOfInput()).thenReturn(true);
        final BowlingScoresGenerator generator = new BowlingScoresGenerator(mInput);
        final List<BowlingScoreResult> bowlingScoreResults = generator.calculateScores();
        assertTrue(bowlingScoreResults.size() == 0);
    }

    @Test
    public void testInputException() throws BowlingScoreInputException {
        final BowlingScoreInput mInput = mock(BowlingScoreInput.class);
        when(mInput.endOfInput()).thenThrow(BowlingScoreInputException.class);
        final BowlingScoresGenerator generator = new BowlingScoresGenerator(mInput);
        final List<BowlingScoreResult> bowlingScoreResults = generator.calculateScores();
        assertTrue(bowlingScoreResults.size() == 0);
    }

    @Test
    public void testInputOneRound() throws BowlingScoreInputException {
        final String input = "0 1 2";
        final BowlingScoreInput mInput = mock(BowlingScoreInput.class);
        final BowlingScoreCalculator mCalculator = mock(BowlingScoreCalculator.class);

        when(mInput.endOfInput()).thenReturn(false, true);
        when(mInput.getNextGame()).thenReturn(input);
        when(mCalculator.calculateScore(input)).thenReturn(new CorrectBowlingScoreResult(input, 3));

        final BowlingScoresGenerator generator = new BowlingScoresGenerator(mInput);
        final List<BowlingScoreResult> bowlingScoreResults = generator.calculateScores();

        assertTrue(bowlingScoreResults.size() == 1);
        final BowlingScoreResult result = bowlingScoreResults.get(0);
        assertTrue(result instanceof CorrectBowlingScoreResult);
        final CorrectBowlingScoreResult cResult = (CorrectBowlingScoreResult) result;
        assertTrue(cResult.getOutput() == 3);
    }

    @Test
    public void testInputTwoRounds() throws BowlingScoreInputException {
        final String input = "0 1 2";
        final String input2 = "3 4 5 6";
        final BowlingScoreInput mInput = mock(BowlingScoreInput.class);
        final BowlingScoreCalculator mCalculator = mock(BowlingScoreCalculator.class);

        when(mInput.endOfInput()).thenReturn(false, false,true);
        when(mInput.getNextGame()).thenReturn(input, input2);
        when(mCalculator.calculateScore(input)).thenReturn(new CorrectBowlingScoreResult(input, 3));
        when(mCalculator.calculateScore(input2)).thenReturn(new ErrorBowlingScoreResult(input, new RuntimeException("Some message")));

        final BowlingScoresGenerator generator = new BowlingScoresGenerator(mInput);
        final List<BowlingScoreResult> bowlingScoreResults = generator.calculateScores();

        assertTrue(bowlingScoreResults.size() == 2);

        final BowlingScoreResult result = bowlingScoreResults.get(0);
        assertTrue(result instanceof CorrectBowlingScoreResult);
        final CorrectBowlingScoreResult cResult = (CorrectBowlingScoreResult) result;
        assertTrue(cResult.getOutput() == 3);

        final BowlingScoreResult result1 = bowlingScoreResults.get(1);
        assertTrue(result1 instanceof ErrorBowlingScoreResult);
        final ErrorBowlingScoreResult cResult1 = (ErrorBowlingScoreResult) result1;
        assertTrue(cResult1.getException() instanceof FrameValidationException);
    }


}
