package co.gdev.bowlingscorecalculator;

import org.testng.annotations.Test;

import java.net.URL;

public class MainTest {

    @Test
    public void testMainRunsWithoutExceptions() {
        URL url = this.getClass().getClassLoader().getResource("testFile.txt");
        Main.main(new String[]{"-f", url.getFile()});
    }

    @Test
    public void testMainInvalidArgsNoExceptions() {
        Main.main(new String[]{"-f", "unknown-file"});
    }

}
