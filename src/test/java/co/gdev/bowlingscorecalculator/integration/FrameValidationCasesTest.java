package co.gdev.bowlingscorecalculator.integration;

import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class FrameValidationCasesTest extends AbstractExceptionCaseTest {

    private static Logger LOG = LoggerFactory.getLogger(FrameValidationCasesTest.class);

    @DataProvider(name = "errorCasesDataProvider")
    public Object[][] data() throws IOException, URISyntaxException {
        return getData("FrameValidationErrorCases.txt");
    }

    @Test(dataProvider = "errorCasesDataProvider")
    public void run(String input) {
        LOG.info("Starting test:: Input - {}, Expected - FrameValidationException", input);
        runTest(input, (Exception e) -> e instanceof FrameValidationException);
    }

}
