package co.gdev.bowlingscorecalculator.integration;

import co.gdev.bowlingscorecalculator.BowlingScoreCalculator;
import co.gdev.bowlingscorecalculator.gameinfo.GameInfo;
import co.gdev.bowlingscorecalculator.gameinfo.NormalGameInfo;
import co.gdev.bowlingscorecalculator.parser.GameFactory;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.ErrorBowlingScoreResult;
import co.gdev.bowlingscorecalculator.scorecalculator.GameScorer;
import co.gdev.bowlingscorecalculator.validation.ValidationController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static org.testng.AssertJUnit.assertTrue;

public abstract class AbstractExceptionCaseTest {

    public Object[][] getData(String fileName) throws IOException, URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource(fileName);
        try (Stream<String> stream = Files.lines(Paths.get(url.toURI()))) {
            return stream
                    .filter(line -> !line.startsWith("//"))
                    .map(entry -> new Object[]{entry})
                    .toArray(Object[][]::new);
        }
    }

    protected void runTest(String input, Predicate<Exception> exceptionFilter) {
        final GameInfo gameInfo = new NormalGameInfo();
        final GameFactory gameFactory = gameInfo.getGameFactory();
        ValidationController validationController = new ValidationController(
                gameInfo.getGameValidationConditions(), gameInfo.getFrameValidationConditions());
        final GameScorer gameScorer = new GameScorer(gameInfo.getFrameScoreCalculators());
        final BowlingScoreCalculator calculator = new BowlingScoreCalculator(gameFactory, validationController, gameScorer);
        final BowlingScoreResult bowlingScoreResult = calculator.calculateScore(input);

        assertTrue(bowlingScoreResult instanceof ErrorBowlingScoreResult);
        ErrorBowlingScoreResult ebsr = (ErrorBowlingScoreResult) bowlingScoreResult;
        assertTrue(exceptionFilter.test(ebsr.getException()));
    }

}
