package co.gdev.bowlingscorecalculator.integration;

import co.gdev.bowlingscorecalculator.BowlingScoreCalculator;
import co.gdev.bowlingscorecalculator.gameinfo.GameInfo;
import co.gdev.bowlingscorecalculator.gameinfo.NormalGameInfo;
import co.gdev.bowlingscorecalculator.parser.GameConstructionException;
import co.gdev.bowlingscorecalculator.parser.GameFactory;
import co.gdev.bowlingscorecalculator.parser.NormalGameFactory;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.CorrectBowlingScoreResult;
import co.gdev.bowlingscorecalculator.scorecalculator.GameScorer;
import co.gdev.bowlingscorecalculator.validation.ValidationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.testng.AssertJUnit.assertTrue;

@Test
public class WorkingCases {

    private static Logger LOG = LoggerFactory.getLogger(WorkingCases.class);

    @DataProvider(name = "dataProviderNoExcept")
    public Object[][] dataProviderNoExcept() throws IOException, URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource("NoExceptCases.txt");
        try (Stream<String> stream = Files.lines(Paths.get(url.toURI()))) {
            return stream
                    .filter(s -> !s.startsWith("//")) // Remove comment lines
                    .map(entry -> {
                String[] split = entry.split("\\|");
                return new Object[]{split[0], split[1]};
            })
            .toArray(Object[][]::new);
        }
    }

    @Test(dataProvider = "dataProviderNoExcept")
    public void run(String input, String expected) throws GameConstructionException {
        LOG.info("Starting test:: Input - {}, Expected - {}", input, expected);
        final GameInfo gameInfo = new NormalGameInfo();
        final GameFactory gameFactory = gameInfo.getGameFactory();
        ValidationController validationController = new ValidationController(
                gameInfo.getGameValidationConditions(), gameInfo.getFrameValidationConditions());
        final GameScorer gameScorer = new GameScorer(gameInfo.getFrameScoreCalculators());
        BowlingScoreCalculator calculator = new BowlingScoreCalculator(gameFactory, validationController, gameScorer);
        final BowlingScoreResult bowlingScoreResult = calculator.calculateScore(input);

        assertTrue(bowlingScoreResult instanceof CorrectBowlingScoreResult);
        CorrectBowlingScoreResult cbsr = (CorrectBowlingScoreResult) bowlingScoreResult;
        Assert.assertEquals(cbsr.getOutput(), Integer.parseInt(expected));
    }

}
