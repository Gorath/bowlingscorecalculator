package co.gdev.bowlingscorecalculator.integration;

import co.gdev.bowlingscorecalculator.BowlingScoreInvalidInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class InvalidInputExceptionCasesTest extends AbstractExceptionCaseTest  {

    private static Logger LOG = LoggerFactory.getLogger(InvalidInputExceptionCasesTest.class);

    @DataProvider(name = "errorCasesDataProvider")
    public Object[][] data() throws IOException, URISyntaxException {
        return getData("InvalidInputCases.txt");
    }

    @Test(dataProvider = "errorCasesDataProvider")
    public void run(String input) {
        LOG.info("Starting test:: Input - {}, Expected - BowlingScoreInvalidInputException", input);
        runTest(input, (Exception e) -> e instanceof BowlingScoreInvalidInputException);
    }

}
