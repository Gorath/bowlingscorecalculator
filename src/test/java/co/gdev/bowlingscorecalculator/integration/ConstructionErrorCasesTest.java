package co.gdev.bowlingscorecalculator.integration;

import co.gdev.bowlingscorecalculator.parser.GameConstructionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class ConstructionErrorCasesTest extends AbstractExceptionCaseTest {

    private static Logger LOG = LoggerFactory.getLogger(WorkingCases.class);

    @DataProvider(name = "errorCasesDataProvider")
    public Object[][] data() throws IOException, URISyntaxException {
        return getData("ConstructionErrorCases.txt");
    }

    @Test(dataProvider = "errorCasesDataProvider")
    public void run(String input) {
        LOG.info("Starting test:: Input - {}, Expected - GameConstructionException", input);
        runTest(input, (Exception e) -> e instanceof GameConstructionException);
    }
}
