package co.gdev.bowlingscorecalculator.validation.game;

import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.validation.GameValidationException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameSizeValidatorTest {

    private final int numberOfFrames = 10;
    private GameSizeValidator validator = new GameSizeValidator(numberOfFrames);

    @DataProvider(name = "invalidGameSizes")
    public Object[][] validValues() { // Returns random selection of invalid game sizes
        return new Object[][] {{0}, {9}, {11}, {15}, {-5}};
    }

    @Test
    public void testValidValue() throws GameValidationException {
        runValidation(10);
    }

    @Test(dataProvider = "invalidGameSizes", expectedExceptions = GameValidationException.class)
    public void testInvalidValues(int value) throws GameValidationException {
        runValidation(value);
    }

    private void runValidation(int value) throws GameValidationException {
        Game mock = mock(Game.class);
        when(mock.getTotalNumberOfFrames()).thenReturn(value);
        validator.validate(mock);
    }


}
