package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TwoBallFrameTotalValidatorTests {
    
    TwoBallFrameTotalValidator validator = new TwoBallFrameTotalValidator();

    @DataProvider(name = "validBowlValues")
    public Object[][] validValues() {
        return new Object[][] {  // Exhaustive list of valid cases, not always viable but here it runs quick enough
                {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10},
                {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7}, {1, 8}, {1, 9},
                {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4}, {2, 5}, {2, 6}, {2, 7}, {2, 8},
                {3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4}, {3, 5}, {3, 6}, {3, 7},
                {4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5}, {4, 6},
                {5, 0}, {5, 1}, {5, 2}, {5, 3}, {5, 4}, {5, 5},
                {6, 0}, {6, 1}, {6, 2}, {6, 3}, {6, 4},
                {7, 0}, {7, 1}, {7, 2}, {7, 3},
                {8, 0}, {8, 1}, {8, 2},
                {9, 0}, {9, 1},
                {10, 0}
        };
    }

    @Test(dataProvider = "validBowlValues")
    public void testValidValues(int firstBallValue, int secondBallValue) throws FrameValidationException {
        runValidation(firstBallValue, secondBallValue);
    }

    @DataProvider(name = "invalidBowlValues")
    public Object[][] invalidValues() {
        return new Object[][] {{0, -1}, {-1, 0}, {-1, -1}, {-50, -50}, {10, 1}, {9, 2}, {2,9}, {1, 10}, {5,6}};
    }

    @Test(dataProvider = "invalidBowlValues", expectedExceptions = FrameValidationException.class)
    public void testTooHighValue(int firstBallValue, int secondBallValue) throws FrameValidationException {
        runValidation(firstBallValue, secondBallValue);
    }

    private void runValidation(int firstBallValue, int secondBallValue) throws FrameValidationException {
        GameContext mockGame = mock(GameContext.class);
        TwoBallFrame mockFrame = mock(TwoBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(10);
        when(mockFrame.getFirstBallScore()).thenReturn(firstBallValue);
        when(mockFrame.getSecondBallScore()).thenReturn(secondBallValue);
        validator.validate(mockGame, mockFrame);
    }

}
