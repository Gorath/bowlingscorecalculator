package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ThreeBallFrameThirdBallValidatorTests {

    ThreeBallFrameThirdBallValidator validator = new ThreeBallFrameThirdBallValidator();

    @Test
    public void strikeAndZeroShouldPass() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(true);
        when(mockFrame.getThirdBallScore()).thenReturn(0);
        validator.validate(mockGame, mockFrame);
    }

    @Test
    public void strikeAndNonZeroShouldPass() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(true);
        when(mockFrame.getThirdBallScore()).thenReturn(5);
        validator.validate(mockGame, mockFrame);
    }

    @Test
    public void spareAndZeroShouldPass() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(false);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(true);
        when(mockFrame.getThirdBallScore()).thenReturn(0);
        validator.validate(mockGame, mockFrame);
    }

    @Test
    public void spareAndNonZeroShouldPass() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(false);
        when(mockFrame.isSpare(noBallsInGame)).thenReturn(true);
        when(mockFrame.getThirdBallScore()).thenReturn(5);
        validator.validate(mockGame, mockFrame);
    }

    @Test
    public void notStrikeNotSpareZeroShouldPass() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(false);
        when(mockFrame.isSpare(noBallsInGame)).thenReturn(false);
        when(mockFrame.getThirdBallScore()).thenReturn(0);
        validator.validate(mockGame, mockFrame);
    }

    @Test(expectedExceptions = FrameValidationException.class)
    public void notStrikeNotSpareNotZeroShouldFail() throws FrameValidationException {
        final int noBallsInGame = 10;
        final GameContext mockGame = mock(GameContext.class);
        ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(noBallsInGame);
        when(mockFrame.isStrike(noBallsInGame)).thenReturn(false);
        when(mockFrame.isSpare(noBallsInGame)).thenReturn(false);
        when(mockFrame.getThirdBallScore()).thenReturn(5);
        validator.validate(mockGame, mockFrame);
    }
}
