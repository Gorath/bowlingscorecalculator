package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ThirdBallScoreValidatorTests {

    ThirdBallScoreValidator validator;

    @BeforeMethod
    public void init() {
        validator = new ThirdBallScoreValidator();
    }

    @DataProvider(name = "validBowlValues")
    public Object[][] validValues() {
        return new Object[][] {{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}};
    }

    @Test(dataProvider = "validBowlValues")
    public void testValidValues(int value) throws FrameValidationException {
        runValidation(value);
    }

    @DataProvider(name = "invalidBowlValues")
    public Object[][] invalidValues() {
        return new Object[][] {{-100}, {-1}, {11}, {53}};
    }

    @Test(dataProvider = "invalidBowlValues", expectedExceptions = FrameValidationException.class)
    public void testTooHighValue(int value) throws FrameValidationException {
        runValidation(value);
    }

    private void runValidation(int value) throws FrameValidationException {
        final GameContext mockGame = mock(GameContext.class);
        final ThreeBallFrame mockFrame = mock(ThreeBallFrame.class);
        when(mockGame.getNumberOfPins()).thenReturn(10);
        when(mockFrame.getThirdBallScore()).thenReturn(value);
        validator.validate(mockGame, mockFrame);
    }

}
