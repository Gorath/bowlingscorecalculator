package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ThreeBallFrameTotalValidatorTests {

    ThreeBallFrameTotalValidator validator = new ThreeBallFrameTotalValidator();

    @DataProvider(name = "validBowlValues")
    public Object[][] validValues() {
        return new Object[][] {  // Exhaustive list of valid cases, not always viable but here its quick enough to keep
                {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10},
                {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7}, {1, 8}, {1, 9},
                {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4}, {2, 5}, {2, 6}, {2, 7}, {2, 8},
                {3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4}, {3, 5}, {3, 6}, {3, 7},
                {4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5}, {4, 6},
                {5, 0}, {5, 1}, {5, 2}, {5, 3}, {5, 4}, {5, 5},
                {6, 0}, {6, 1}, {6, 2}, {6, 3}, {6, 4},
                {7, 0}, {7, 1}, {7, 2}, {7, 3},
                {8, 0}, {8, 1}, {8, 2},
                {9, 0}, {9, 1},
                {10, 0}
        };
    }

    @DataProvider(name = "invalidBowlValues")
    public Object[][] invalidValues() {
        return new Object[][] {{0, -1}, {-1, 0}, {-1, -1}, {-50, -50}, {10, 1}, {9, 2}, {2,9}, {1, 10}, {5,6}};
    }

    @Test(dataProvider = "validBowlValues")
    public void strikeSecondAndThirdBallTotalInBounds(int ball1, int ball2) throws FrameValidationException {
        testBall2And3(ball1, ball2);
    }

    @Test(dataProvider = "invalidBowlValues", expectedExceptions = FrameValidationException.class)
    public void strikeSecondAndThirdBallTotalOutOfBounds(int ball2, int ball3) throws FrameValidationException {
        testBall2And3(ball2, ball3);
    }

    private void testBall2And3(int ball2, int ball3) throws FrameValidationException {
        final int totalNoPins = 10;
        final GameContext mockGame = mock(GameContext.class);
        final ThreeBallFrame mock = mock(ThreeBallFrame.class);

        when(mockGame.getNumberOfPins()).thenReturn(totalNoPins);
        when(mock.isStrike(totalNoPins)).thenReturn(true);
        when(mock.getSecondBallScore()).thenReturn(ball2);
        when(mock.getThirdBallScore()).thenReturn(ball3);

        validator.validate(mockGame, mock);
    }

    @Test(dataProvider = "validBowlValues")
    public void notStrikeFirstAndSecondBallTotalInBounds(int ball1, int ball2) throws FrameValidationException {
        testBall1And2(ball1, ball2);
    }

    @Test(dataProvider = "invalidBowlValues", expectedExceptions = FrameValidationException.class)
    public void notStrikeFirstAndSecondBallTotalOutOfBounds(int ball1, int ball2) throws FrameValidationException {
        testBall1And2(ball1, ball2);
    }

    private void testBall1And2(int ball1, int ball2) throws FrameValidationException {
        final int totalNoPins = 10;
        final GameContext mockGame = mock(GameContext.class);
        final ThreeBallFrame mock = mock(ThreeBallFrame.class);

        when(mockGame.getNumberOfPins()).thenReturn(totalNoPins);
        when(mock.isStrike(totalNoPins)).thenReturn(false);
        when(mock.getFirstBallScore()).thenReturn(ball1);
        when(mock.getSecondBallScore()).thenReturn(ball2);

        validator.validate(mockGame, mock);
    }

}
