package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;
import co.gdev.bowlingscorecalculator.validation.frame.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class FrameValidationControllerTests {

    // Example reflection sanity check with ThreeBallFrame.class
    @Test
    public void checkCorrectFrameValidationMocksCalledForThreeBallFrame() throws FrameValidationException {
        FirstBallScoreValidator mock1 = mock(FirstBallScoreValidator.class);
        SecondBallScoreValidator mock2 = mock(SecondBallScoreValidator.class);
        ThirdBallScoreValidator mock3 = mock(ThirdBallScoreValidator.class);
        ThreeBallFrameThirdBallValidator mock4 = mock(ThreeBallFrameThirdBallValidator.class);
        ThreeBallFrameTotalValidator mock5 = mock(ThreeBallFrameTotalValidator.class);
        TwoBallFrameTotalValidator       mock6 = mock(TwoBallFrameTotalValidator.class);

        when(mock1.appliesTo()).thenReturn(new FirstBallScoreValidator().appliesTo());
        when(mock2.appliesTo()).thenReturn(new SecondBallScoreValidator().appliesTo());
        when(mock3.appliesTo()).thenReturn(new ThirdBallScoreValidator().appliesTo());
        when(mock4.appliesTo()).thenReturn(new ThreeBallFrameThirdBallValidator().appliesTo());
        when(mock5.appliesTo()).thenReturn(new ThreeBallFrameTotalValidator().appliesTo());
        when(mock6.appliesTo()).thenReturn(new TwoBallFrameTotalValidator().appliesTo());

        ArrayList<FrameValidator> frameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6));


        Game mockGame = mock(Game.class);
        ThreeBallFrame threeBallFrame = mock(ThreeBallFrame.class);
        when(mockGame.getFrames()).thenReturn(Collections.singletonList(threeBallFrame));

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);

        verify(mock1, times(1)).validate(mockGame, threeBallFrame);
        verify(mock2, times(1)).validate(mockGame, threeBallFrame);
        verify(mock3, times(1)).validate(mockGame, threeBallFrame);
        verify(mock4, times(1)).validate(mockGame, threeBallFrame);
        verify(mock5, times(1)).validate(mockGame, threeBallFrame);
        verify(mock6, times(0)).validate(any(), any());
    }

    // Example reflection sanity check with TwoBallFrame.class
    @Test
    public void checkCorrectFrameValidationMocksCalledForTwoBallFrame() throws FrameValidationException {
        FirstBallScoreValidator          mock1 = mock(FirstBallScoreValidator.class);
        SecondBallScoreValidator         mock2 = mock(SecondBallScoreValidator.class);
        ThirdBallScoreValidator          mock3 = mock(ThirdBallScoreValidator.class);
        ThreeBallFrameThirdBallValidator mock4 = mock(ThreeBallFrameThirdBallValidator.class);
        ThreeBallFrameTotalValidator     mock5 = mock(ThreeBallFrameTotalValidator.class);
        TwoBallFrameTotalValidator       mock6 = mock(TwoBallFrameTotalValidator.class);

        when(mock1.appliesTo()).thenReturn(new FirstBallScoreValidator().appliesTo());
        when(mock2.appliesTo()).thenReturn(new SecondBallScoreValidator().appliesTo());
        when(mock3.appliesTo()).thenReturn(new ThirdBallScoreValidator().appliesTo());
        when(mock4.appliesTo()).thenReturn(new ThreeBallFrameThirdBallValidator().appliesTo());
        when(mock5.appliesTo()).thenReturn(new ThreeBallFrameTotalValidator().appliesTo());
        when(mock6.appliesTo()).thenReturn(new TwoBallFrameTotalValidator().appliesTo());

        ArrayList<FrameValidator> frameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6));


        Game mockGame = mock(Game.class);
        TwoBallFrame twoBallFrame = mock(TwoBallFrame.class);
        when(mockGame.getFrames()).thenReturn(Collections.singletonList(twoBallFrame));

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);

        verify(mock1, times(1)).validate(mockGame, twoBallFrame);
        verify(mock2, times(1)).validate(mockGame, twoBallFrame);
        verify(mock3, times(0)).validate(any(), any());
        verify(mock4, times(0)).validate(any(), any());
        verify(mock5, times(0)).validate(any(), any());
        verify(mock6, times(1)).validate(mockGame, twoBallFrame);
    }



    /**
     * Few more examples for reflection checking
     */
    @Test
    @SuppressWarnings("unchecked")
    public void sanityCheckThreeBallFrameClass() throws FrameValidationException {
        FrameValidator mock1 = mock(FrameValidator.class);
        FrameValidator mock2 = mock(FrameValidator.class);
        FrameValidator mock3 = mock(FrameValidator.class);
        FrameValidator mock4 = mock(FrameValidator.class);
        FrameValidator mock5 = mock(FrameValidator.class);
        FrameValidator mock6 = mock(FrameValidator.class);

        when(mock1.appliesTo()).thenReturn(Frame.class);
        when(mock2.appliesTo()).thenReturn(Frame.class);
        when(mock3.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock4.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock5.appliesTo()).thenReturn(ThreeBallFrame.class);
        when(mock6.appliesTo()).thenReturn(ThreeBallFrame.class);

        ArrayList<FrameValidator> frameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6));


        Game mockGame = mock(Game.class);
        ThreeBallFrame threeBallFrame = mock(ThreeBallFrame.class);
        when(mockGame.getFrames()).thenReturn(Collections.singletonList(threeBallFrame));

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);

        verify(mock1, times(1)).validate(mockGame, threeBallFrame);
        verify(mock2, times(1)).validate(mockGame, threeBallFrame);
        verify(mock3, times(0)).validate(any(), any());
        verify(mock4, times(0)).validate(any(), any());
        verify(mock5, times(1)).validate(mockGame, threeBallFrame);
        verify(mock6, times(1)).validate(mockGame, threeBallFrame);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void sanityCheckTwoBallFrameClass() throws FrameValidationException {
        FrameValidator mock1 = mock(FrameValidator.class);
        FrameValidator mock2 = mock(FrameValidator.class);
        FrameValidator mock3 = mock(FrameValidator.class);
        FrameValidator mock4 = mock(FrameValidator.class);
        FrameValidator mock5 = mock(FrameValidator.class);
        FrameValidator mock6 = mock(FrameValidator.class);

        when(mock1.appliesTo()).thenReturn(Frame.class);
        when(mock2.appliesTo()).thenReturn(Frame.class);
        when(mock3.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock4.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock5.appliesTo()).thenReturn(ThreeBallFrame.class);
        when(mock6.appliesTo()).thenReturn(ThreeBallFrame.class);

        ArrayList<FrameValidator> frameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6));


        Game mockGame = mock(Game.class);
        TwoBallFrame twoBallFrame = mock(TwoBallFrame.class);
        when(mockGame.getFrames()).thenReturn(Collections.singletonList(twoBallFrame));

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);

        verify(mock1, times(1)).validate(mockGame, twoBallFrame);
        verify(mock2, times(1)).validate(mockGame, twoBallFrame);
        verify(mock3, times(1)).validate(mockGame, twoBallFrame);
        verify(mock4, times(1)).validate(mockGame, twoBallFrame);
        verify(mock5, times(0)).validate(any(), any());
        verify(mock6, times(0)).validate(any(), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void sanityCheckFrameClass() throws FrameValidationException {
        FrameValidator mock1 = mock(FrameValidator.class);
        FrameValidator mock2 = mock(FrameValidator.class);
        FrameValidator mock3 = mock(FrameValidator.class);
        FrameValidator mock4 = mock(FrameValidator.class);
        FrameValidator mock5 = mock(FrameValidator.class);
        FrameValidator mock6 = mock(FrameValidator.class);

        when(mock1.appliesTo()).thenReturn(Frame.class);
        when(mock2.appliesTo()).thenReturn(Frame.class);
        when(mock3.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock4.appliesTo()).thenReturn(TwoBallFrame.class);
        when(mock5.appliesTo()).thenReturn(ThreeBallFrame.class);
        when(mock6.appliesTo()).thenReturn(ThreeBallFrame.class);

        ArrayList<FrameValidator> frameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3, mock4, mock5, mock6));


        Game mockGame = mock(Game.class);
        Frame frame = mock(Frame.class);
        when(mockGame.getFrames()).thenReturn(Collections.singletonList(frame));

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);

        verify(mock1, times(1)).validate(mockGame, frame);
        verify(mock2, times(1)).validate(mockGame, frame);
        verify(mock3, times(0)).validate(any(), any());
        verify(mock4, times(0)).validate(any(), any());
        verify(mock5, times(0)).validate(any(), any());
        verify(mock6, times(0)).validate(any(), any());
    }

    @Test(expectedExceptions = FrameValidationException.class)
    @SuppressWarnings("unchecked")
    public void testFrameValidationExceptionIsThrown() throws FrameValidationException {
        Game mockGame = mock(Game.class);
        Frame mockFrame = mock(Frame.class);
        FrameValidator mockValidator = mock(FrameValidator.class);
        List<FrameValidator> frameValidators = Collections.singletonList(mockValidator);

        when(mockGame.getFrames()).thenReturn(Collections.singletonList(mockFrame));
        when(mockValidator.appliesTo()).thenReturn(Frame.class);
        doThrow(new FrameValidationException("ConstructionErrorCasesTest")).when(mockValidator).validate(mockGame, mockFrame);

        FrameValidationController validationController = new FrameValidationController(frameValidators);
        validationController.runFrameValidation(mockGame);
    }
}
