package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Game;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class GameValidationControllerImplTests {


    @Test
    public void checkAllGameValidationMocksCalled() throws GameValidationException {
        Game mockGame = mock(Game.class);
        GameValidator mock1 = mock(GameValidator.class);
        GameValidator mock2 = mock(GameValidator.class);
        GameValidator mock3 = mock(GameValidator.class);
        ArrayList<GameValidator> gameValidators = new ArrayList<>(Arrays.asList(mock1, mock2, mock3));

        GameValidationController validationController = new GameValidationController(gameValidators);
        validationController.validateGame(mockGame);

        verify(mock1, times(1)).validate(mockGame);
        verify(mock2, times(1)).validate(mockGame);
        verify(mock3, times(1)).validate(mockGame);
    }

    @Test
    public void checkEmptyListReturnsSuccessfullyNoExceptions() throws GameValidationException {
        Game mockGame = mock(Game.class);
        List<GameValidator> gameValidators = Collections.emptyList();

        GameValidationController validationController = new GameValidationController(gameValidators);
        validationController.validateGame(mockGame);

    }

    @Test(expectedExceptions = GameValidationException.class)
    public void testGameValidationExceptionIsThrown() throws GameValidationException {
        Game mockGame = mock(Game.class);
        GameValidator mockValidator = mock(GameValidator.class);

        doThrow(new GameValidationException("ConstructionErrorCasesTest")).when(mockValidator).validate(mockGame);
        List<GameValidator> gameValidators = Collections.singletonList(mockValidator);

        GameValidationController validationController = new GameValidationController(gameValidators);
        validationController.validateGame(mockGame);
    }

}
