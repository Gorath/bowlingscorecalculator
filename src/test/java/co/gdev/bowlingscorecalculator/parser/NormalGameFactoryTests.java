package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.gameinfo.GameInfo;
import co.gdev.bowlingscorecalculator.gameinfo.NormalGameInfo;
import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class NormalGameFactoryTests {

    GameInfo gameInfo = new NormalGameInfo();
    NormalGameFactory factory = new NormalGameFactory(gameInfo.getNumberOfPins(), gameInfo.getFrameCreators());

    private void checkFrameScoresMatch(Frame frame, int first, int second) {
        if (!(frame instanceof TwoBallFrame)) {
            fail("Frame " + frame.getFrameNumber() + " should be a two ball frame");
        }
        assertEquals(frame.getFirstBallScore(), first);
        assertEquals(frame.getSecondBallScore(), second);
    }

    private void checkFrameScoresMatch(Frame frame, int first, int second, int third) {
        if (!(frame instanceof ThreeBallFrame)) {
            fail("The 10th frame should be a three ball frame.");
        }
        ThreeBallFrame tbf = (ThreeBallFrame) frame;
        assertEquals(tbf.getFirstBallScore(), first);
        assertEquals(tbf.getSecondBallScore(), second);
        assertEquals(tbf.getThirdBallScore(), third);
    }


    @Test
    public void testPartialScoreCardEvenNumber() throws GameConstructionException {
        Integer[] vals = new Integer[]{1,9,1,9};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), vals[0], vals[1]);
        checkFrameScoresMatch(frames.get(1), vals[2], vals[3]);
        checkFrameScoresMatch(frames.get(2), 0, 0);
        checkFrameScoresMatch(frames.get(3), 0, 0);
        checkFrameScoresMatch(frames.get(4), 0, 0);
        checkFrameScoresMatch(frames.get(5), 0, 0);
        checkFrameScoresMatch(frames.get(6), 0, 0);
        checkFrameScoresMatch(frames.get(7), 0, 0);
        checkFrameScoresMatch(frames.get(8), 0, 0);
        checkFrameScoresMatch(frames.get(9), 0, 0, 0);

    }

    @Test
    public void testPartialScoreCardOddNumber() throws GameConstructionException {
            Integer[] vals = new Integer[]{1,9,1,9,4,5,6};
            Game game = factory.create(vals);
            List<Frame> frames = game.getFrames();
            assertEquals(frames.size(), 10, "Number of frames should be 10");
            checkFrameScoresMatch(frames.get(0), vals[0], vals[1]);
            checkFrameScoresMatch(frames.get(1), vals[2], vals[3]);
            checkFrameScoresMatch(frames.get(2), vals[4], vals[5]);
            checkFrameScoresMatch(frames.get(3), vals[6], 0); // This should fill second with 0
            checkFrameScoresMatch(frames.get(4), 0, 0);
            checkFrameScoresMatch(frames.get(5), 0, 0);
            checkFrameScoresMatch(frames.get(6), 0, 0);
            checkFrameScoresMatch(frames.get(7), 0, 0);
            checkFrameScoresMatch(frames.get(8), 0, 0);
            checkFrameScoresMatch(frames.get(9), 0, 0, 0);

    }
    @Test
    public void testPartialScoreCardWithStrike() throws GameConstructionException {
        Integer[] vals = new Integer[]{1,9,1,9,10,5,6};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), vals[0], vals[1]);
        checkFrameScoresMatch(frames.get(1), vals[2], vals[3]);
        checkFrameScoresMatch(frames.get(2), vals[4], 0);
        checkFrameScoresMatch(frames.get(3), vals[5], vals[6]);
        checkFrameScoresMatch(frames.get(4), 0, 0);
        checkFrameScoresMatch(frames.get(5), 0, 0);
        checkFrameScoresMatch(frames.get(6), 0, 0);
        checkFrameScoresMatch(frames.get(7), 0, 0);
        checkFrameScoresMatch(frames.get(8), 0, 0);
        checkFrameScoresMatch(frames.get(9), 0, 0, 0);
    }

    @Test
    public void testEmptyScorecard() throws GameConstructionException {
        Integer[] vals = new Integer[]{};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), 0, 0);
        checkFrameScoresMatch(frames.get(1), 0, 0);
        checkFrameScoresMatch(frames.get(2), 0, 0);
        checkFrameScoresMatch(frames.get(3), 0, 0);
        checkFrameScoresMatch(frames.get(4), 0, 0);
        checkFrameScoresMatch(frames.get(5), 0, 0);
        checkFrameScoresMatch(frames.get(6), 0, 0);
        checkFrameScoresMatch(frames.get(7), 0, 0);
        checkFrameScoresMatch(frames.get(8), 0, 0);
        checkFrameScoresMatch(frames.get(9), 0, 0, 0);
    }

    @Test
    public void testCheckFullScoreCard() throws GameConstructionException {
        Integer[] vals = new Integer[]{1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,10,9,1};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), vals[0], vals[1]);
        checkFrameScoresMatch(frames.get(1), vals[2], vals[3]);
        checkFrameScoresMatch(frames.get(2), vals[4], vals[5]);
        checkFrameScoresMatch(frames.get(3), vals[6], vals[7]);
        checkFrameScoresMatch(frames.get(4), vals[8], vals[9]);
        checkFrameScoresMatch(frames.get(5), vals[10], vals[11]);
        checkFrameScoresMatch(frames.get(6), vals[12], vals[13]);
        checkFrameScoresMatch(frames.get(7), vals[14], vals[15]);
        checkFrameScoresMatch(frames.get(8), vals[16], vals[17]);
        checkFrameScoresMatch(frames.get(9), vals[18], vals[19], vals[20]);
    }

    @Test
    public void testCheckFullScoreCardWithStrikes() throws GameConstructionException {
        Integer[] vals = new Integer[]{1,2,3,4,5,6,10,9,1,2,3,4,5,10,8,9,10,9,1};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), vals[0], vals[1]);
        checkFrameScoresMatch(frames.get(1), vals[2], vals[3]);
        checkFrameScoresMatch(frames.get(2), vals[4], vals[5]);
        checkFrameScoresMatch(frames.get(3), vals[6], 0);
        checkFrameScoresMatch(frames.get(4), vals[7], vals[8]);
        checkFrameScoresMatch(frames.get(5), vals[9], vals[10]);
        checkFrameScoresMatch(frames.get(6), vals[11], vals[12]);
        checkFrameScoresMatch(frames.get(7), vals[13], 0);
        checkFrameScoresMatch(frames.get(8), vals[14], vals[15]);
        checkFrameScoresMatch(frames.get(9), vals[16], vals[17], vals[18]);
    }

    @Test
    public void testCheckFullScoreCardWithAll() throws GameConstructionException {
        Integer[] vals = new Integer[]{10,10,10,10,10,10,10,10,10,10,10,10};
        Game game = factory.create(vals);
        List<Frame> frames = game.getFrames();
        assertEquals(frames.size(), 10, "Number of frames should be 10");
        checkFrameScoresMatch(frames.get(0), vals[0], 0);
        checkFrameScoresMatch(frames.get(1), vals[1], 0);
        checkFrameScoresMatch(frames.get(2), vals[2], 0);
        checkFrameScoresMatch(frames.get(3), vals[3], 0);
        checkFrameScoresMatch(frames.get(4), vals[4], 0);
        checkFrameScoresMatch(frames.get(5), vals[5], 0);
        checkFrameScoresMatch(frames.get(6), vals[6], 0);
        checkFrameScoresMatch(frames.get(7), vals[7], 0);
        checkFrameScoresMatch(frames.get(8), vals[8], 0);
        checkFrameScoresMatch(frames.get(9), vals[9], vals[10], vals[11]);
    }

    @Test(expectedExceptions = GameConstructionException.class)
    public void testTooManyElementsAllStrikes() throws GameConstructionException {
        Integer[] vals = new Integer[]{10,10,10,10,10,10,10,10,10,10,10,10, 1};
        factory.create(vals);
    }

    @Test(expectedExceptions = GameConstructionException.class)
    public void testTooManyElementsAllNormalFrames() throws GameConstructionException {
        Integer[] vals = new Integer[]{1,2,3,4,5,6,10,9,1,2,3,4,5,10,8,9,10,9,1, 1};
        factory.create(vals);
    }
}
