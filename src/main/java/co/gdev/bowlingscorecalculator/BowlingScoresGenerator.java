package co.gdev.bowlingscorecalculator;

import co.gdev.bowlingscorecalculator.gameinfo.GameInfo;
import co.gdev.bowlingscorecalculator.gameinfo.NormalGameInfo;
import co.gdev.bowlingscorecalculator.input.BowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.BowlingScoreInputException;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.scorecalculator.GameScorer;
import co.gdev.bowlingscorecalculator.validation.ValidationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BowlingScoresGenerator {

    private static Logger LOG = LoggerFactory.getLogger(BowlingScoresGenerator.class);

    private final BowlingScoreCalculator calculator;
    private final BowlingScoreInput input;

    BowlingScoresGenerator(BowlingScoreInput input) {
        this(input, new NormalGameInfo());
    }

    private BowlingScoresGenerator(BowlingScoreInput input, GameInfo gameInfo) {
        this.input = input;
        this.calculator = new BowlingScoreCalculator(
                gameInfo.getGameFactory(),
                new ValidationController(gameInfo.getGameValidationConditions(), gameInfo.getFrameValidationConditions()),
                new GameScorer(gameInfo.getFrameScoreCalculators())
        );
    }

    List<BowlingScoreResult> calculateScores() {
        List<BowlingScoreResult> returnValues = new ArrayList<>();
        try {
            while (!input.endOfInput()) {
                final String nextGame = input.getNextGame();
                returnValues.add(calculator.calculateScore(nextGame));
            }
        } catch (BowlingScoreInputException e) {
            LOG.error("Error Occurred while reading input. {}", e);
        }
        return returnValues;
    }
}
