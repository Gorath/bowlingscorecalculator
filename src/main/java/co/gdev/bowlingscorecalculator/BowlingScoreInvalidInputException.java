package co.gdev.bowlingscorecalculator;

public class BowlingScoreInvalidInputException extends Exception {

    public BowlingScoreInvalidInputException(String message) {
        super(message);
    }

    public BowlingScoreInvalidInputException(String message, Throwable th) {
        super(message, th);
    }

}
