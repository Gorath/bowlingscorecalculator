package co.gdev.bowlingscorecalculator;

import co.gdev.bowlingscorecalculator.cli.CliParser;
import co.gdev.bowlingscorecalculator.input.BowlingScoreInput;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOG.info("Args are {}", Arrays.toString(args));
        final BowlingScoreInput inputSource = CliParser.getInputSource(args);
        if (inputSource != null) {
            final BowlingScoresGenerator bowlingScoresCalculator = new BowlingScoresGenerator(inputSource);
            final List<BowlingScoreResult> bowlingScoreResults = bowlingScoresCalculator.calculateScores();
            outputResults(bowlingScoreResults);
        } else {
            LOG.warn("Could not generate correct input source.  Terminating.");
        }

    }

    private static void outputResults(List<BowlingScoreResult> bowlingScoreResults) {
        StringBuilder builder = new StringBuilder();
        bowlingScoreResults.forEach(elem -> builder.append(elem.toString()).append("\n"));
        LOG.info("The full list of results are as follows:: \n{}", builder.toString());
    }


}