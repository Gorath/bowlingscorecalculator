package co.gdev.bowlingscorecalculator.result;

public class CorrectBowlingScoreResult extends BowlingScoreResult {

    private final int output;

    public CorrectBowlingScoreResult(String input, int output) {
        super(input);
        this.output = output;
    }

    public int getOutput() {
        return output;
    }

    @Override
    public String toString() {
        return "CorrectBowlingScoreResult{ Input - '" + getInput() + "' :: Result - " + output + " }";
    }
}
