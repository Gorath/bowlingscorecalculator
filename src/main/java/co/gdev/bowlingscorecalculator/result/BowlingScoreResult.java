package co.gdev.bowlingscorecalculator.result;

public abstract class BowlingScoreResult {

    private final String input;

    protected BowlingScoreResult(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }
}
