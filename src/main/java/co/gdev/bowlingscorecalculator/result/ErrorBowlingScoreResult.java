package co.gdev.bowlingscorecalculator.result;

public class ErrorBowlingScoreResult extends BowlingScoreResult {

    private final Exception exception;

    public ErrorBowlingScoreResult(String input, Exception e) {
        super(input);
        this.exception = e;
    }

    public Exception getException() {
        return exception;
    }

    @Override
    public String toString() {
        return "ErrorBowlingScoreResult{ Input - '" + getInput() + "' :: Result - " + exception.getClass().getSimpleName() + " - " + exception.getMessage() + " }";
    }
}
