package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NormalThreeBallFrameScoreCalculator implements FrameScoreCalculator<ThreeBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(NormalThreeBallFrameScoreCalculator.class);

    @Override
    public int calculateScore(ThreeBallFrame frame, GameContext context) throws ScoreCalculationException {
        int total = 0;
        final int firstBallScore = frame.getFirstBallScore();
        final int secondBallScore = frame.getSecondBallScore();
        final int thirdBallScore = frame.getThirdBallScore();
        total += firstBallScore + secondBallScore + thirdBallScore;
        LOG.debug("Frame total for frame {} is {}.", this, total);
        return total;
    }

    @Override
    public Class<? extends ThreeBallFrame> appliesTo() {
        return ThreeBallFrame.class;
    }

}
