package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;

public interface FrameScoreCalculator<T extends Frame> {

    int calculateScore(T frame, GameContext context) throws ScoreCalculationException;

    Class<? extends T> appliesTo();

}
