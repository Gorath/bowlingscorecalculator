package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;

import java.util.List;

public class GameScorer {

    private final List<FrameScoreCalculator> calculators;

    public GameScorer(List<FrameScoreCalculator> calculators) {
        this.calculators = calculators;
    }

    public int scoreGame(GameContext gameContext) throws ScoreCalculationException {
        final List<Frame> frames = gameContext.getFrames();
        final int calculatorsSize = calculators.size();
        final int framesSize = frames.size();

        checkNumberOfFramesAndCalculatorsMatch(calculatorsSize, framesSize);
        return getGameTotal(gameContext, frames, calculatorsSize);

    }

    private int getGameTotal(GameContext gameContext, List<Frame> frames, int calculatorsSize) throws ScoreCalculationException {
        int total = 0;
        for (int i = 0; i < calculatorsSize; i++) {
            final FrameScoreCalculator calculator = calculators.get(i);
            final Frame frame = frames.get(i);
            if (!calculator.appliesTo().isAssignableFrom(frame.getClass())) {
                final String errorMessage = String.format("Calculator type mismatch.  Expected: %s.  " +
                        "Was: %s", calculator.appliesTo().getSimpleName(), frame.getClass().getSimpleName());
                throw new ScoreCalculationException(errorMessage);
            }
            total += calculator.calculateScore(frame, gameContext);
        }
        return total;
    }

    private void checkNumberOfFramesAndCalculatorsMatch(int calculatorsSize, int framesSize) throws ScoreCalculationException {
        if (calculatorsSize != framesSize) {
            final String errorMessage = String.format("Game setup incorrectly. The number of Frames and Frame Score " +
                    "Calculators didn't match. Frames: %s.  Frame Score Calculators: %s.", framesSize, calculatorsSize);
            throw new ScoreCalculationException(errorMessage);
        }
    }

}
