package co.gdev.bowlingscorecalculator.scorecalculator;

public class ScoreCalculationException extends Exception {

    public ScoreCalculationException(String message) {
        super(message);
    }

    public ScoreCalculationException(String message, Throwable th) {
        super(message, th);
    }
}
