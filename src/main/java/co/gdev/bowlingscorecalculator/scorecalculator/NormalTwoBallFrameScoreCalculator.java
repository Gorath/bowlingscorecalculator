package co.gdev.bowlingscorecalculator.scorecalculator;

import co.gdev.bowlingscorecalculator.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NormalTwoBallFrameScoreCalculator implements FrameScoreCalculator<TwoBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(NormalTwoBallFrameScoreCalculator.class);

    @Override
    public int calculateScore(TwoBallFrame frame, GameContext context) throws ScoreCalculationException {
        int total = 0;
        final int frameNumber = frame.getFrameNumber();
        final int firstBallScore = frame.getFirstBallScore();
        final int secondBallScore = frame.getSecondBallScore();

        try {
            // Add the balls from this frame
            total += firstBallScore + secondBallScore;
            LOG.debug("{}: total with first and second balls of this frame included {}", toString(), total);

            // Add any bonus score if required
            final int numberOfPins = context.getNumberOfPins();
            if (frame.isStrike(numberOfPins) || frame.isSpare(numberOfPins)) {
                BallFetcher bf = new TwoBallsNextFrameFetcher(); // Non-expensive construction
                if (context.getFrameByNumber(frameNumber + 1) instanceof ThreeBallFrame) {
                    bf = new ThreeFrameNextBallsFetcher();
                }
                int nextBallScore = bf.getNextBallScore(context, frameNumber);
                total += nextBallScore;
                LOG.debug("{}: Bonus spare/strike first ball: {} -- Total: {}", toString(), nextBallScore, total);
                if (frame.isStrike(numberOfPins)) {
                    int nextNextBallScore = bf.getNextNextBallScore(context, frameNumber);
                    total += nextNextBallScore;
                    LOG.debug("{}: Bonuns strike second ball: {} -- Total: {}", toString(), nextNextBallScore, total);
                }
            }
        } catch (InvalidFrameFetchException e) {
            throw new ScoreCalculationException("Tried to fetch an invalid frame.", e);
        }
        return total;
    }

    @Override
    public Class<? extends TwoBallFrame> appliesTo() {
        return TwoBallFrame.class;
    }

    private interface BallFetcher {

        int getNextBallScore(GameContext gameContext, int currentFrameNumber) throws InvalidFrameFetchException;

        int getNextNextBallScore(GameContext gameContext, int currentFrameNumber) throws InvalidFrameFetchException;

    }

    private abstract class AbstractBallFetcher implements BallFetcher {
        @Override
        public int getNextBallScore(GameContext gameContext, int currentFrameNumber) throws InvalidFrameFetchException {
            return gameContext.getFrameByNumber(currentFrameNumber+1).getFirstBallScore();
        }
    }

    private class TwoBallsNextFrameFetcher extends AbstractBallFetcher {
        /**
         * If next frame is a two ball frame we need to decide if we can get the second ball from that frame (if
         * its a non-strike frame) or if we need to get the first ball of the frame after
         */
        @Override
        public int getNextNextBallScore(GameContext gameContext, int currentFrameNumber) throws InvalidFrameFetchException {
            Frame frameByNumber = gameContext.getFrameByNumber(currentFrameNumber + 1);
            if (!frameByNumber.isStrike(gameContext.getNumberOfPins())) {
                return frameByNumber.getSecondBallScore();
            } else {
                return gameContext.getFrameByNumber(currentFrameNumber+2).getFirstBallScore();
            }
        }
    }

    private class ThreeFrameNextBallsFetcher extends AbstractBallFetcher {
        /**
         * If the next frame is a three ball frame we need to get the second ball of that frame
         */
        @Override
        public int getNextNextBallScore(GameContext gameContext, int currentFrameNumber) throws InvalidFrameFetchException {
            return gameContext.getFrameByNumber(currentFrameNumber + 1).getSecondBallScore();
        }
    }
}
