package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Game;

public interface GameValidator {

    void validate(Game game) throws GameValidationException;

}
