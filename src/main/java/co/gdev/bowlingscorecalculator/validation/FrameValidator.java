package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;

public interface FrameValidator<T extends Frame> {

    void validate(GameContext gameContext, T frame) throws FrameValidationException;

    Class<? extends T> appliesTo();

}
