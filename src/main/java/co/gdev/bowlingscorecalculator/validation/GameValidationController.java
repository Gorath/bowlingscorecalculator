package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Game;

import java.util.Collections;
import java.util.List;

public class GameValidationController {

    private List<GameValidator> gameValidators;

    public GameValidationController(List<GameValidator> gameValidators) {
        this.gameValidators = gameValidators;

        if (this.gameValidators == null) {
            this.gameValidators = Collections.emptyList();
        }
    }

    public void validateGame(Game theGame) throws GameValidationException {
        // Could of used stream API if there was no exception thrown
        for (GameValidator gameValidator : gameValidators) {
            gameValidator.validate(theGame);
        }
    }
}
