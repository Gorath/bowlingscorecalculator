package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Game;

import java.util.List;

public class ValidationController {

    private final FrameValidationController frameValidationController;
    private final GameValidationController gameValidationController;

    public ValidationController(List<GameValidator> gameValidationConditions, List<FrameValidator> frameValidationConditions) {
        frameValidationController = new FrameValidationController(frameValidationConditions);
        gameValidationController = new GameValidationController(gameValidationConditions);
    }

    public void runValidation(Game theGame) throws FrameValidationException, GameValidationException {
        gameValidationController.validateGame(theGame);
        frameValidationController.runFrameValidation(theGame);
    }

}
