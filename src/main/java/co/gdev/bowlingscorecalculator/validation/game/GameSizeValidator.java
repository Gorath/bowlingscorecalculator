package co.gdev.bowlingscorecalculator.validation.game;

import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.validation.GameValidationException;
import co.gdev.bowlingscorecalculator.validation.GameValidator;

public class GameSizeValidator implements GameValidator {

    private final int expectedNumberOfFrames;

    public GameSizeValidator(int expectedNumberOfFrames) {
        this.expectedNumberOfFrames = expectedNumberOfFrames;
    }

    @Override
    public void validate(Game game) throws GameValidationException {
        int numFrames = game.getTotalNumberOfFrames();
        if (numFrames != expectedNumberOfFrames) {
            throw new GameValidationException("Invalid number of frames. Expected: " + expectedNumberOfFrames +
                    ". Actual: " + numFrames+ ".");
        }
    }

}
