package co.gdev.bowlingscorecalculator.validation;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.Game;

import java.util.Collections;
import java.util.List;

public class FrameValidationController {
    private List<FrameValidator> frameValidators;

    public FrameValidationController(List<FrameValidator> frameValidators) {
        this.frameValidators = frameValidators;

        if (this.frameValidators == null) {
            this.frameValidators = Collections.emptyList();
        }
    }

    // Would of used the stream API but this throws a checked exception
    public void runFrameValidation(Game theGame) throws FrameValidationException {
        for (Frame frame : theGame.getFrames()) {
            final Class<? extends Frame> frameClass = frame.getClass();
            for (FrameValidator frameValidator : frameValidators) {
                if (frameValidator.appliesTo().isAssignableFrom(frameClass)) {
                    frameValidator.validate(theGame, frame);
                }
            }
        }
    }
}
