package co.gdev.bowlingscorecalculator.validation;

/**
 * This is an unchecked exception.
 * This is a bad idea for production systems but it keeps the code cleaner and means I can use the stream API
 * for the validation controller
 */
public class FrameValidationException extends Exception {

    public FrameValidationException(String message) {
        super(message);
    }

}
