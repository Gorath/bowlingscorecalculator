package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThirdBallScoreValidator implements FrameValidator<ThreeBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(SecondBallScoreValidator.class);

    @Override
    public void validate(GameContext gameContext, ThreeBallFrame frame) throws FrameValidationException {
        final int totalNumberOfPins = gameContext.getNumberOfPins();
        int thirdBallScore = frame.getThirdBallScore();
        if (thirdBallScore < 0 || thirdBallScore > totalNumberOfPins) {
            final String errorMessage = String.format("Score for third ball of frame %s is too large.  Must be in " +
                            "range 0 <= x <= {}.", frame, totalNumberOfPins);
            LOG.info(errorMessage);
            throw new FrameValidationException("Failed ThirdBallScoreValidator: " + errorMessage);
        }
    }

    @Override
    public Class<ThreeBallFrame> appliesTo() {
        return ThreeBallFrame.class;
    }

}