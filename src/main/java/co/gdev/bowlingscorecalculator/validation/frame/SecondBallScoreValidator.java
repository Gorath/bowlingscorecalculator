package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecondBallScoreValidator implements FrameValidator<Frame> {

    private static Logger LOG = LoggerFactory.getLogger(SecondBallScoreValidator.class);

    @Override
    public void validate(GameContext gameContext, Frame frame) throws FrameValidationException {
        final int totalNumberOfPins = gameContext.getNumberOfPins();
        int secondBallScore = frame.getSecondBallScore();
        if (secondBallScore < 0 || secondBallScore > totalNumberOfPins) {
            final String errorMessage = String.format("Score for second ball of frame %s is too large.  Must be in " +
                            "range 0 <= x <= %s.", frame, totalNumberOfPins);
            LOG.info(errorMessage);
            throw new FrameValidationException("Failed SecondBallScoreValidator: " + errorMessage);
        }
    }

    @Override
    public Class<Frame> appliesTo() {
        return Frame.class;
    }
}