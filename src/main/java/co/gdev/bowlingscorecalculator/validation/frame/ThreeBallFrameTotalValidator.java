package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This could (should) be split into two classes
 */
public class ThreeBallFrameTotalValidator implements FrameValidator<ThreeBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(ThreeBallFrameTotalValidator.class);

    @Override
    public void validate(GameContext gameContext, ThreeBallFrame frame) throws FrameValidationException {
        final int numberOfPins = gameContext.getNumberOfPins();

        if (frame.isStrike(numberOfPins)) {
            checkSecondTwoBallScores(frame, numberOfPins);
        } else {
            checkFirstTwoBallScores(frame, numberOfPins);
        }

    }

    private void checkFirstTwoBallScores(ThreeBallFrame frame, int numberOfPins) throws FrameValidationException {
        int total = frame.getFirstBallScore() + frame.getSecondBallScore();
        String errorMessage = "Three ball frame " + frame + ".  As first ball is not strike the sum of the first two" +
                "shots must be in range 0 <= x <= " + numberOfPins + ".  Total {" + total + "} is out of bounds." ;
        checkBallSumIsInRange0ToNumPinsOrThrow(total, numberOfPins, errorMessage);
    }

    private void checkSecondTwoBallScores(ThreeBallFrame frame, int numberOfPins) throws FrameValidationException {
        // If second ball is strike no range check is necessary
        if (!frame.isSecondBallStrike(numberOfPins)) {
            int total = frame.getSecondBallScore() + frame.getThirdBallScore();
            String errorMessage = "Frame " + frame + ".  As first ball is strike and second ball is not, sum of " +
                    "balls two and three must be less than " + numberOfPins + ", but sum is " + total + ". Please check input.";
            checkBallSumIsInRange0ToNumPinsOrThrow(total, numberOfPins, errorMessage);
        }
    }

    private void checkBallSumIsInRange0ToNumPinsOrThrow(int total, int numberOfPins, String errorMessage) throws FrameValidationException {
        if (total < 0 || total > numberOfPins) {
            logAndThrow(errorMessage);
        }
    }

    private void logAndThrow(String errorMessage) throws FrameValidationException {
        LOG.info(errorMessage);
        throw new FrameValidationException("Failed ThreeBallFrameTotalValidator: " + errorMessage);
    }

    @Override
    public Class<ThreeBallFrame> appliesTo() {
        return ThreeBallFrame.class;
    }
}
