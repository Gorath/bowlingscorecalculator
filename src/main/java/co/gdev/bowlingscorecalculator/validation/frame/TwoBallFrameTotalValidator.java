package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class only validates the sum of the scores is in range.
 * The case where ball 1 = 15, ball 2 = -8 will pass this but is handled by
 * {@link co.gdev.bowlingscorecalculator.validation.frame.FirstBallScoreValidator}
 * {@link co.gdev.bowlingscorecalculator.validation.frame.SecondBallScoreValidator}
 */
public class TwoBallFrameTotalValidator implements FrameValidator<TwoBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(TwoBallFrameTotalValidator.class);

    @Override
    public void validate(GameContext gameContext, TwoBallFrame frame) throws FrameValidationException {
        final int totalNumberOfPins = gameContext.getNumberOfPins();
        final int total = frame.getFirstBallScore() + frame.getSecondBallScore();
        if (total < 0 || total > totalNumberOfPins) {
            final String errorMessage = String.format("Total %s for frame %s is out of range.  Must be in range " +
                            "0 <= total <= %s.", total, frame, totalNumberOfPins);
            LOG.info(errorMessage);
            throw new FrameValidationException("Failed TwoBallFrameTotalValidator: " + errorMessage);
        }
    }

    @Override
    public Class<TwoBallFrame> appliesTo() {
        return TwoBallFrame.class;
    }
}