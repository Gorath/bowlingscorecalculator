package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirstBallScoreValidator implements FrameValidator<Frame> {

    private static Logger LOG = LoggerFactory.getLogger(FirstBallScoreValidator.class);

    @Override
    public void validate(GameContext gameContext, Frame frame) throws FrameValidationException {
        final int totalNumberOfPins = gameContext.getNumberOfPins();
        int firstBallScore = frame.getFirstBallScore();
        if (firstBallScore < 0 || firstBallScore > totalNumberOfPins) {
            final String errorMessage = String.format("Score for first ball of frame %s is out of range.  Must be in " +
                            "range 0 <= x <= %s.", frame, totalNumberOfPins);
            LOG.info(errorMessage);
           throw new FrameValidationException("Failed FirstBallScoreValidator: " + errorMessage);
        }
    }

    @Override
    public Class<Frame> appliesTo() {
        return Frame.class;
    }

}
