package co.gdev.bowlingscorecalculator.validation.frame;

import co.gdev.bowlingscorecalculator.model.GameContext;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;
import co.gdev.bowlingscorecalculator.validation.FrameValidationException;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreeBallFrameThirdBallValidator implements FrameValidator<ThreeBallFrame> {

    private static Logger LOG = LoggerFactory.getLogger(ThreeBallFrameThirdBallValidator.class);

    @Override
    public void validate(GameContext gameContext, ThreeBallFrame frame) throws FrameValidationException {
        final int numberOfPins = gameContext.getNumberOfPins();
        final int thirdBallScore = frame.getThirdBallScore();

        if (thirdBallScore != 0 && !frame.isStrike(numberOfPins) && !frame.isSpare(numberOfPins)) {
            // If we don't get a strike or a spare the third ball should always be 0
            final String errorMessage = String.format("Third ball of frame %s must be 0 if the frame is not a strike or " +
                    "a spare frame.  Was: %s. Number of pins: %s", frame, thirdBallScore, numberOfPins);
                    LOG.info(errorMessage);
            throw new FrameValidationException("Failed ThreeBallFrameThirdBallValidator: " + errorMessage);
        }

    }

    @Override
    public Class<ThreeBallFrame> appliesTo() {
        return ThreeBallFrame.class;
    }

}
