package co.gdev.bowlingscorecalculator.validation;

public class GameValidationException extends Exception {

    public GameValidationException(String message) {
        super(message);
    }
}
