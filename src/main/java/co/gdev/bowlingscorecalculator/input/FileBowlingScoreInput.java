package co.gdev.bowlingscorecalculator.input;

import java.io.*;

public class FileBowlingScoreInput extends BufferedReaderBowlingScoreInput {

    public FileBowlingScoreInput(String fileName) throws FileNotFoundException {
        super(new BufferedReader(new FileReader(new File(fileName))));
    }

}
