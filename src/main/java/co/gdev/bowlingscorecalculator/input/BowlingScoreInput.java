package co.gdev.bowlingscorecalculator.input;

public interface BowlingScoreInput {

    /**
     * Returns the next game as a space separated string
     */
    String getNextGame() throws BowlingScoreInputException;

    /**
     * Returns true if the end of the input has been reached
     */
    boolean endOfInput() throws BowlingScoreInputException;

}
