package co.gdev.bowlingscorecalculator.input;

import java.io.*;

public class StringBowlingScoreInput extends BufferedReaderBowlingScoreInput {

    public StringBowlingScoreInput(String score) {
        super(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(score.getBytes()))));
    }

}
