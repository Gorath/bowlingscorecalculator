package co.gdev.bowlingscorecalculator.input;

public class BowlingScoreInputException extends Exception {

    BowlingScoreInputException(String message, Throwable th) {
        super(message, th);
    }

}
