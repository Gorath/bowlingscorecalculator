package co.gdev.bowlingscorecalculator.input;

import java.io.BufferedReader;
import java.io.IOException;

public class BufferedReaderBowlingScoreInput implements BowlingScoreInput {

    private final BufferedReader reader;

    public BufferedReaderBowlingScoreInput(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public String getNextGame() throws BowlingScoreInputException {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new BowlingScoreInputException("Error occurred reading line.", e);
        }
    }

    @Override
    public boolean endOfInput() throws BowlingScoreInputException {
        try {
            return !reader.ready();
        } catch (IOException e) {
            throw new BowlingScoreInputException("Error occurred checking if reader is ready.", e);
        }
    }

}
