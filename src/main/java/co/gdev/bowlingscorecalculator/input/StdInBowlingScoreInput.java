package co.gdev.bowlingscorecalculator.input;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class StdInBowlingScoreInput extends BufferedReaderBowlingScoreInput {

    private static Logger LOG = LoggerFactory.getLogger(StdInBowlingScoreInput.class);

    public StdInBowlingScoreInput() {
       super(new BufferedReader(new InputStreamReader(System.in)));
    }

    @Override
    public String getNextGame() throws BowlingScoreInputException {
        LOG.info("Enter score for next game:");
        return super.getNextGame();
    }
    @Override
    public boolean endOfInput() {
        return false;
    }
}
