package co.gdev.bowlingscorecalculator.cli;

import co.gdev.bowlingscorecalculator.input.BowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.FileBowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.StdInBowlingScoreInput;
import co.gdev.bowlingscorecalculator.input.StringBowlingScoreInput;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

public class CliParser {

    private static Logger LOG = LoggerFactory.getLogger(CliParser.class);

    /**
     * Generates the input source corresponding to the args provided
     */
    public static BowlingScoreInput getInputSource(String[] args) {
        final CommandLine commandLine = parseArgs(args);
        final Option[] options = commandLine.getOptions();

        if (tooFewArgs(options)) {
            // If no args selected then use std in as default
            return  new StdInBowlingScoreInput();
        } else if (tooManyArgs(options)) {
            return null; // Don't know what you want to do
        }
        return generateBowlingInputSource(options[0]);
    }

    /**
     *  Parses command line options
     */
    private static CommandLine parseArgs(String[] args) {
        try {
            return CliParserOptions.parseOpts(args);
        } catch (ParseException e) {
            LOG.error("Error parsing the command line arguments.\n" + e.toString());
            System.exit(1);
        }
        return null; // This will never happen
    }

    /**
     *  Check for too many args (more than 1 specified input source)
     */
    private static boolean tooManyArgs(Option[] options) {
        boolean actualNumArgsTooLarge = options != null && options.length > 1;
        if (actualNumArgsTooLarge) {
            LOG.error("Multiple options specified - Please select only one option");
        }
        return actualNumArgsTooLarge;
    }

    /**
     *  Checks for too few args (no input source specified)
     */
    private static boolean tooFewArgs(Option[] options) {
        boolean tooFewArgs = options == null || options.length == 0;
        if (tooFewArgs) {
            LOG.info("No options specified - defaulting to sin");
        }
        return tooFewArgs;
    }

    /**
     * Takes an option from the command line and generates the corresponding
     * {@link co.gdev.bowlingscorecalculator.input.BowlingScoreInput}
     * @param option The option to check
     * @return the corresponding input source
     */
    private static BowlingScoreInput generateBowlingInputSource(Option option) {
        String opt = option.getOpt();
        if (opt.equals("f")) { // File
            try {
                return new FileBowlingScoreInput(option.getValue());
            } catch (FileNotFoundException e) {
                LOG.error("File {} not found.", option.getValue());
                return null;
            }
        } else if (opt.equals("s")) { // String
            return new StringBowlingScoreInput(option.getValue());
        }
        // Sin is only option left
        return new StdInBowlingScoreInput();

    }
}
