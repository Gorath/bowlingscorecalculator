package co.gdev.bowlingscorecalculator.cli;

import org.apache.commons.cli.*;

public class CliParserOptions {

    public static CommandLine parseOpts(String[] args) throws ParseException {
        final CommandLineParser parser = new DefaultParser();
        return parser.parse(createOptions(), args);
    }

    private static Options createOptions() {
        final Options options = new Options();
        options.addOption("f", "file", true, "The input file to parse");
        options.addOption("s", "string", true, "The string to parse");
        options.addOption("sin", "sin", false, "Read games from stdin");
        return options;
    }

}
