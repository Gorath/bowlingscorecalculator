package co.gdev.bowlingscorecalculator;

import co.gdev.bowlingscorecalculator.model.Game;
import co.gdev.bowlingscorecalculator.parser.GameFactory;
import co.gdev.bowlingscorecalculator.result.BowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.CorrectBowlingScoreResult;
import co.gdev.bowlingscorecalculator.result.ErrorBowlingScoreResult;
import co.gdev.bowlingscorecalculator.scorecalculator.GameScorer;
import co.gdev.bowlingscorecalculator.validation.ValidationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class BowlingScoreCalculator {

    private static Logger LOG = LoggerFactory.getLogger(BowlingScoreCalculator.class);

    private final GameFactory gameFactory;
    private final ValidationController validationController;
    private final GameScorer gameScorer;

    public BowlingScoreCalculator(GameFactory gameFactory, ValidationController validationController, GameScorer gameScorer) {
        this.gameFactory = gameFactory;
        this.validationController = validationController;
        this.gameScorer = gameScorer;

    }

    public BowlingScoreResult calculateScore(String input) {
        try {
            LOG.info("Calculating score for input {}.", input);
            final Integer[] values = parseGameScoresToIntegerArray(input);
            final Game theGame = gameFactory.create(values);
            validationController.runValidation(theGame);
            final int score = gameScorer.scoreGame(theGame);
            LOG.info("Score for input '{}' is {}.", input, score);
            return new CorrectBowlingScoreResult(input, score);
        } catch (Exception e) {
            // Catch all exceptions - we always want a correctly formed result out of the function
            LOG.info("Caught exception while trying to calculate score. {}", e.toString());
            return new ErrorBowlingScoreResult(input, e);
        }
    }

    private static Integer[] parseGameScoresToIntegerArray(String line) throws BowlingScoreInvalidInputException {
        chceckForEmptyInput(line);
        try {
            String[] elem = line.trim().split(" ");
            return Arrays.stream(elem).map(Integer::parseInt).toArray(Integer[]::new);
        } catch (NumberFormatException e) {
            throw new BowlingScoreInvalidInputException("Please make sure all input values are integers.", e);
        }
    }

    private static void chceckForEmptyInput(String line) throws BowlingScoreInvalidInputException {
        if (line.trim().isEmpty()) {
            throw new BowlingScoreInvalidInputException("Input Invalid.  Must contain at least one integer.");
        }
    }
}
