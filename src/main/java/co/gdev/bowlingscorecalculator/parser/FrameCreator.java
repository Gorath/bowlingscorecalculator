package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.model.Frame;

import java.util.List;

public interface FrameCreator {
    /**
     * Creates a new frame of specified type from the input args
     * @param scores the bowling scores
     * @param frameList the list of currently calculated frames N.B. the new frame should be added to the end of this list
     * @param frameNumber the frame number of the new frame to create
     * @param i the offset in the scores array for the first ball of this frame
     * @return the new offset for the scores array for the next score that isn't used yet
     */
    int constructFrame(Integer[] scores, List<Frame> frameList, int frameNumber, int i);

    /**
     * Creates a new empty frame (values for all bowls are zero)
     * @param frameNumber the frame number of the frame to create
     * @param frameList the frame list to add the frame to
     */
    void constructEmptyFrame(int frameNumber, List<Frame> frameList);
}
