package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.Game;

import java.util.ArrayList;
import java.util.List;

/**
 * A Normal Game consists of nine TwoBallFrames and a ThreeBallFrame on the end
 */
public class NormalGameFactory implements GameFactory {


    private final int numberOfPins;
    private final List<FrameCreator> frameCreators;

    public NormalGameFactory(int numberOfPins, List<FrameCreator> frameCreators) {
        this.numberOfPins = numberOfPins;
        this.frameCreators = frameCreators;
    }

    /**
     * Creates a Game from the input list of scores using the game creators
     * If there are not enough balls to finish creating the game, enters empty frames for the remaining creators
     * @param scores the scores for the game
     * @return the Game object which mirrors the list of scores
     * @throws GameConstructionException if there are too many balls for the given frames
     */
    public Game create(Integer[] scores) throws GameConstructionException {
        List<Frame> frameList = new ArrayList<>();
        int frameNumber = 1;
        for (int i = 0; i < scores.length; i++) {

            if (frameNumber > frameCreators.size()) {
                // error too many balls for the number and type of frames
                throw new GameConstructionException("There were too many balls in the game.");
            }

            final FrameCreator frameCreator = frameCreators.get(frameNumber - 1);
            i = frameCreator.constructFrame(scores, frameList, frameNumber, i);
            frameNumber++;

        }

        fillInEmptyFrames(frameNumber, frameList, frameCreators);
        return Game.of(numberOfPins, frameList);
    }

    /**
     * Modifies frame list to fill in 0 score bowls for the remaining frames
     */
    private static void fillInEmptyFrames(int frameNumber, List<Frame> frameList, List<FrameCreator> frameCreators) {
        for (int newFrameNumber = frameNumber; newFrameNumber <=frameCreators.size(); newFrameNumber++) {
            final FrameCreator frameCreator = frameCreators.get(newFrameNumber - 1);
            frameCreator.constructEmptyFrame(newFrameNumber, frameList);
        }
    }

}
