package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.TwoBallFrame;

import java.util.List;

/**
 * Creates a two ball frame.  If the first score is not equal to the number of pins in each frame
 * then then a second ball is taken form the list
 */
public class TwoBallFrameCreator implements FrameCreator {

    private final int numberOfPins;

    public TwoBallFrameCreator(int numberOfPins) {
        this.numberOfPins = numberOfPins;
    }

    public int constructFrame(Integer[] scores, List<Frame> frameList, int frameNumber, int i) {
        // Each TwoBall frame has two balls max
        int first = scores[i];
        int second = 0;

        // If the first score is not a strike and we are not at end of input
        // There is another ball to add to this frame
        if (first != numberOfPins && i != scores.length - 1) {
            second = scores[i+1];
            i++;
        }
        frameList.add(TwoBallFrame.of(frameNumber, first, second));
        return i;
    }

    @Override
    public void constructEmptyFrame(int frameNumber, List<Frame> frameList) {
        frameList.add(TwoBallFrame.of(frameNumber, 0, 0));
    }

}
