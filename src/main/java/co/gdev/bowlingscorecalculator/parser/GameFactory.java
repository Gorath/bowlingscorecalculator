package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.model.Game;

public interface GameFactory {

    /**
     * Takes as input an integer list of scores and outputs a game object
     * @param scores the scores for the game
     * @return the constructed game object for that game
     * @throws GameConstructionException if the game is malformed
     */
    Game create(Integer[] scores) throws GameConstructionException;

}
