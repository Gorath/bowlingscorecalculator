package co.gdev.bowlingscorecalculator.parser;

public class GameConstructionException extends Exception {

    public GameConstructionException(String message) {
        super(message);
    }
}
