package co.gdev.bowlingscorecalculator.parser;

import co.gdev.bowlingscorecalculator.model.Frame;
import co.gdev.bowlingscorecalculator.model.ThreeBallFrame;

import java.util.List;

public class ThreeBallFrameCreator implements FrameCreator {

    public int constructFrame(Integer[] scores, List<Frame> frameList, int frameNumber, int i) {
        // Each frame has two balls max
        int first = scores[i];
        int second = 0;
        int third = 0;

        // There are always at least two balls in a normal three ball frame so no need to check if strike
        if (i != scores.length - 1) {
            second = scores[i+1];
            i++;
        }

        // If there is a third ball in this frame add it - errors are picked up in validation if
        // Frame is not a strike or spare and this value is non-zero
        if (i != scores.length - 1) {
            third = scores[i+1];
            i++;
        }

        frameList.add(ThreeBallFrame.of(frameNumber, first, second, third));
        return i;
    }

    @Override
    public void constructEmptyFrame(int frameNumber, List<Frame> frameList) {
        frameList.add(ThreeBallFrame.of(frameNumber, 0,0,0));
    }
}
