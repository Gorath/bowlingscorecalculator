package co.gdev.bowlingscorecalculator.gameinfo;

import co.gdev.bowlingscorecalculator.parser.*;
import co.gdev.bowlingscorecalculator.scorecalculator.FrameScoreCalculator;
import co.gdev.bowlingscorecalculator.scorecalculator.NormalThreeBallFrameScoreCalculator;
import co.gdev.bowlingscorecalculator.scorecalculator.NormalTwoBallFrameScoreCalculator;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import co.gdev.bowlingscorecalculator.validation.GameValidator;
import co.gdev.bowlingscorecalculator.validation.frame.*;
import co.gdev.bowlingscorecalculator.validation.game.GameSizeValidator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NormalGameInfo implements GameInfo {

    /**
     * Number of pins in the game
     */
    @Override
    public int getNumberOfPins() {
        return 10;
    }

    /**
     * Game validation conditions to apply to this game
     */
    @Override
    public List<GameValidator> getGameValidationConditions() {
        return Collections.singletonList(new GameSizeValidator(getFrameCreators().size()));
    }

    /**
     * Frame validation conditions returned get applied to every frame in the game if
     * appliesTo() type is assignable from the frame type
     */
    @Override
    public List<FrameValidator> getFrameValidationConditions() {
        return Arrays.asList(
                new FirstBallScoreValidator(),
                new SecondBallScoreValidator(),
                new ThirdBallScoreValidator(),
                new TwoBallFrameTotalValidator(),
                new ThreeBallFrameTotalValidator(),
                new ThreeBallFrameThirdBallValidator()
        );
    }

    /**
     * This is essentially the structure of the game
     * The normal game is nine two ball frames followed by a three ball frame
     * @return
     */
    @Override
    public List<FrameCreator> getFrameCreators() {
        final TwoBallFrameCreator twoBallFrameCreator = new TwoBallFrameCreator(getNumberOfPins());
        final ThreeBallFrameCreator threeBallFrameCreator = new ThreeBallFrameCreator();
        return Arrays.asList(
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                twoBallFrameCreator,
                threeBallFrameCreator
                );
    }

    /**
     * Returns the game factory to generate a game of this type
     * This game has ten two ball frames followed by a three ball frame
     */
    @Override
    public GameFactory getGameFactory() {
        return new NormalGameFactory(getNumberOfPins(), getFrameCreators());
    }

    /**
     * Returns the score calculators to apply to the game
     * Must match the number of frames in the game
     */
    @Override
    public List<FrameScoreCalculator> getFrameScoreCalculators() {
        final NormalTwoBallFrameScoreCalculator normalTwoBallFrameScoreCalculator = new NormalTwoBallFrameScoreCalculator();
        final NormalThreeBallFrameScoreCalculator normalThreeBallFrameScoreCalculator = new NormalThreeBallFrameScoreCalculator();
        return Arrays.asList(
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalTwoBallFrameScoreCalculator,
                normalThreeBallFrameScoreCalculator
                );
    }

}