package co.gdev.bowlingscorecalculator.gameinfo;

import co.gdev.bowlingscorecalculator.parser.FrameCreator;
import co.gdev.bowlingscorecalculator.parser.GameFactory;
import co.gdev.bowlingscorecalculator.scorecalculator.FrameScoreCalculator;
import co.gdev.bowlingscorecalculator.validation.FrameValidator;
import co.gdev.bowlingscorecalculator.validation.GameValidator;

import java.util.List;

public interface GameInfo {

    /**
     * Number of pins in the game
     */
    int getNumberOfPins();

    /**
     * Game validation conditions to apply to this game
     */
    List<GameValidator> getGameValidationConditions();

    /**
     * Frame validation conditions returned get applied to every frame in the game if
     * appliesTo() type is assignable from the frame type
     */
    List<FrameValidator> getFrameValidationConditions();

    /**
     * This is essentially the structure of the game
     * The normal game is nine two ball frames followed by a three ball frame
     * @return
     */
    List<FrameCreator> getFrameCreators();

    /**
     * Returns the game factory to generate a game of this type
     * This game has ten two ball frames followed by a three ball frame
     */
    GameFactory getGameFactory();

    /**
     * Returns the score calculators to apply to the game
     * Must match the number of frames in the game
     */
    List<FrameScoreCalculator> getFrameScoreCalculators();

}
