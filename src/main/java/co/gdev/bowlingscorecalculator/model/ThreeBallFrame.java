package co.gdev.bowlingscorecalculator.model;

public class ThreeBallFrame implements Frame {

    private final int frameNo;
    private final int first;
    private final int second;
    private final int third;

    public static ThreeBallFrame of(int frameNo, int first, int second, int third) {
        return new ThreeBallFrame(frameNo, first, second, third);
    }

    public ThreeBallFrame(int frameNo, int first, int second, int third) {
        this.frameNo = frameNo;
        this.first = first;
        this.second = second;
        this.third = third;

    }

    @Override
    public int getFrameNumber() {
        return frameNo;
    }

    @Override
    public int getFirstBallScore() {
        return first;
    }

    @Override
    public int getSecondBallScore() {
        return second;
    }

    public int getThirdBallScore() { return third; }

    public boolean isSecondBallStrike(int numberOfPins) {
        return second == numberOfPins;
    }

    @Override
    public String toString() {
        return "Fr(" + first + "|" + second + "|" + third + ")";
    }

}
