package co.gdev.bowlingscorecalculator.model;

public class InvalidFrameFetchException extends Exception {

    public InvalidFrameFetchException(String message, Throwable th) {
        super(message, th);
    }

}
