package co.gdev.bowlingscorecalculator.model;

public interface Frame {

    int getFrameNumber();

    int getFirstBallScore();

    int getSecondBallScore();

    /**
     * Returns true if the first ball of the frame is a 10
     */
    default boolean isStrike(int totalNoPins) {
        return getFirstBallScore() == totalNoPins;
    }

    /**
     * Returns true if the sum of the first two balls of the frame is 10 and !isStrike
     */
    default boolean isSpare(int totalNoPins) {
        return !isStrike(totalNoPins) && getFirstBallScore() + getSecondBallScore() == totalNoPins;
    }

}
