package co.gdev.bowlingscorecalculator.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Game implements GameContext {

    private static Logger LOG = LoggerFactory.getLogger(Game.class);

    private final int noOfPins;
    private final List<Frame> theFrames;

    public static Game of(int noOfPins, List<Frame> theFrames) {
        return new Game(noOfPins, theFrames);
    }

    private Game(int noOfPins, List<Frame> theFrames) {
        this.noOfPins = noOfPins;
        this.theFrames = theFrames;
    }

    @Override
    public int getNumberOfPins() {
        return noOfPins;
    }

    @Override
    public Frame getFrameByNumber(int frameNo) throws InvalidFrameFetchException {
        try {
            return theFrames.get(frameNo-1); // -1 for zero index
        } catch (IndexOutOfBoundsException e) {
            LOG.warn("Could not find frame, returning empty frame");
            throw new InvalidFrameFetchException("Invalid frame number: " + frameNo +" for game " + toString(), e);
        }
    }

    public List<Frame> getFrames() {
        return theFrames;
    }

    public int getTotalNumberOfFrames() {
        return theFrames.size();
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        theFrames.forEach(
                frame -> builder
                    .append(frame.toString())
                    .append(" ")
        );
        return "Game{ " + builder.toString() + "}";
    }
}
