package co.gdev.bowlingscorecalculator.model;

import java.util.List;

public interface GameContext {

    int getNumberOfPins();

    Frame getFrameByNumber(int frameNo) throws InvalidFrameFetchException;

    List<Frame> getFrames();

}
