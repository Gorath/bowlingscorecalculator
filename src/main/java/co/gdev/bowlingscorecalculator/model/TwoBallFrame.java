package co.gdev.bowlingscorecalculator.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwoBallFrame implements Frame {

    private static Logger LOG = LoggerFactory.getLogger(TwoBallFrame.class);

    private final int frameNo;
    private final int first;
    private final int second;

    public static TwoBallFrame of(int frameNo, int first, int second) {
        return new TwoBallFrame(frameNo, first, second);
    }

    public TwoBallFrame(int frameNo, int first, int second) {
        this.frameNo = frameNo;
        this.first = first;
        this.second = second;

    }

    @Override
    public int getFrameNumber() {
        return frameNo;
    }

    @Override
    public int getFirstBallScore() {
        return first;
    }

    @Override
    public int getSecondBallScore() {
        return second;
    }

    @Override
    public String toString() {
        if (first == 10) {
            return "Fr(10)";
        }
        return "Fr(" + first + "|" + second + ")";
    }


}
