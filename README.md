# Bowling Score Calculator

## What Is the Bowling Score Calculator?

> "He who bowls, needs to know his score" - Me (Just now)

This is a program to calculate a bowling score from user provided input.

A game input is a space delimited String.  For example "10 1 2 3 4".

If there is a score of 10 for the first ball of a frame **the frame is finished** and the next score in the list will 
be the start of the next frame.

So from the example above:

* Frame 1: Ball1(10)
* Frame 2: Ball1(2), Ball2(1)
* Frame 3: Ball1(3), Ball2(4) 

If a full game is not entered then the remaining bowls will be scored at 0 and a total gathered.

Validation is performed on the input so if a frame has too many pins, too many scores are entered or another error 
condition is met, then an error will be output rather than a score.

## Notes To The Reader

The solution as presented is quite large.  However, it is extremely flexible.  As an exercise to the reader try editing 
the NormalGameInfo object to:

* Change the number of pins in the game (that's right - with 15 pins rather than 10 you can now score 450 for a perfect game!)
* Add a new validation condition so if a frame has a first ball score of 5 the game is invalid
* Add a new score calculator so the first ball of the first frame is doubled in value

More advanced modifications include:

* Modifying the number of frames in the game (E.g. add five more `TwoBallFrame` objects for games with 15 frames)
* Design your own types of frames/score calculators and seamlessly wire them in

## Requirements

* Java 8
* Maven

## Usage

Run the command `mvn package` from the root directory to build and test the solution. 

There are three modes of operation for the program, each is outlined below.

#### StdIn

Flag: `-sin`

Running the following command will provide an interactive session where you can enter scores:

`java -jar target/bowling-score-calculator-1.0.0-jar-with-dependencies.jar -sin`

Test by entering the values '5 2 3 4' at the prompt - should output 14.

#### File Input

Flag: `-f "filename"`

To perform a bulk input you can pass in a file and the engine will calculate scores for all the games in the file.  Games are seperated by new lines. 

To run with one of the test files you can use the command:

`java -jar target/bowling-score-calculator-1.0.0-jar-with-dependencies.jar -f target/test-classes/testFile.txt`

#### String Input

Flag: `-s "Game Scores"`

The last mode is string input.  You can directly provide the game on the command line:

`java -jar target/bowling-score-calculator-1.0.0-SNAPSHOT-jar-with-dependencies.jar -s 
"10 10 10 10 10 10 10 10 10 10 10 10"`


Congratulations - you just bowled a perfect game!
